/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package saac;

import java.awt.Color;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Jose
 */

public class Simulator extends javax.swing.JFrame {
    private static final String version = "2015.01.30";
    
    private static HashMap<String, Component> componentMap;
    
    private static TreeMap<String, Team> teams = new TreeMap<String, Team>();
    private static Match[] matches = new Match[51];
    
    private static Statistics statisticsFrame = null;
    
    private final static Comparator<Team> ascGroups = new Comparator<Team>(){
        public int compare(Team b1, Team b2){
            
            if(b1 != null && b2 == null)
                return 1;
            else if (b1 == null && b2 != null)
                return -1;
            else if (b1 == null && b2 == null)
                return 0;
            
            if(b1.getPoints()-b2.getPoints()>0)
                return 1;
            else if(b1.getPoints()-b2.getPoints()<0)
                return -1;
            
            Match[] common = getMatches(b1,b2);
            int pointsB1 = 0;
            int pointsB2 = 0;
            int goalDiffA = 0;
            int goalDiffB = 0;
            int goalSumA = 0;
            int goalSumB = 0;
            for(int i = 0; i < common.length; i++) { 
                if(common[i] != null) {
                    if(common[i].getLocal().equals(b1)){
                        pointsB1 += common[i].getPointsLocal();
                        pointsB2 += common[i].getPointsVisitor();
                        goalDiffA = Math.max(common[i].getGoalsLocal()-common[i].getGoalsVisitor(), goalDiffA);
                        goalDiffB = Math.max(common[i].getGoalsVisitor()-common[i].getGoalsLocal(), goalDiffB);
                        goalSumA += common[i].getGoalsLocal();
                        goalSumB += common[i].getGoalsVisitor();
                    }
                    else {
                        pointsB2 += common[i].getPointsLocal();
                        pointsB1 += common[i].getPointsVisitor();
                        goalDiffB = Math.max(common[i].getGoalsLocal()-common[i].getGoalsVisitor(), goalDiffB);
                        goalDiffA = Math.max(common[i].getGoalsVisitor()-common[i].getGoalsLocal(), goalDiffA);
                        goalSumB += common[i].getGoalsLocal();
                        goalSumA += common[i].getGoalsVisitor();
                    }
                }
            }

            //Desempate 1. Mayor número de puntos obtenidos en los partidos jugados entre los equipos en cuestión.
            if(pointsB1 - pointsB2 > 0)
                return 1;
            else if(pointsB1 - pointsB2 < 0)
                return -1;
            
            //Desempate 2. Mejor diferencia de gol producto de los partidos jugados entre los equipos en cuestión.
            if(goalDiffA - goalDiffB > 0)
                return 1;
            else if(goalDiffA - goalDiffB < 0)
                return -1;
            
            //Desempate 3. Mayor número de goles marcados en los partidos jugados entre los equipos en cuestión.
            if(goalSumA - goalSumB > 0)
                return 1;
            else if(goalSumA - goalSumB < 0)
                return -1;
            
            Match[] matchesA = getMatches(b1);
            Match[] matchesB = getMatches(b2);
            goalDiffA = 0;
            goalDiffB = 0;
            goalSumA = 0;
            goalSumB = 0;
            for(int i = 0; i < matchesA.length; i++) { 
                if(matchesA[i] != null) {
                    if(matchesA[i].getLocal().equals(b1)){
                        goalDiffA = Math.max(matchesA[i].getGoalsLocal()-matchesA[i].getGoalsVisitor(), goalDiffA);
                        goalSumA += matchesA[i].getGoalsLocal();
                    }
                    else {
                        goalDiffA = Math.max(matchesA[i].getGoalsVisitor()-matchesA[i].getGoalsLocal(), goalDiffA);
                        goalSumA += matchesA[i].getGoalsVisitor();
                    }
                }
            }
            for(int i = 0; i < matchesB.length; i++) { 
                if(matchesB[i] != null) {
                    if(matchesB[i].getLocal().equals(b2)){
                        goalDiffB = Math.max(matchesB[i].getGoalsVisitor()-matchesB[i].getGoalsLocal(), goalDiffB);
                        goalSumB += matchesB[i].getGoalsVisitor();
                    }
                    else {
                        goalDiffB = Math.max(matchesB[i].getGoalsLocal()-matchesB[i].getGoalsVisitor(), goalDiffB);
                        goalSumB += matchesB[i].getGoalsLocal();
                    }
                }
            }                        
            //Desempate 4. Mejor diferencia de gol en todos los partidos de grupo.
            if(goalDiffA - goalDiffB > 0)
                return 1;
            else if(goalDiffA - goalDiffB < 0)
                return -1;
            
            //Desempate 5. Mayor número de goles marcados en todos los partidos de grupo.
            if(goalSumA - goalSumB > 0)
                return 1;
            else if(goalSumA - goalSumB < 0)
                return -1;
            
            //Desempate 6. NO APLICA. La conducta Fair Play en todos los partidos de grupo (De acuerdo a lo definido en el Anexo C.5.1 del reglamento del torneo).
            //Desempate 7. Posición en el Ranking de coeficientes de la UEFA con que se inició este torneo.
            if(b1.getUefaCoefficient() - b2.getUefaCoefficient() > 0)
                return 1;
            else if(b1.getUefaCoefficient() - b2.getUefaCoefficient() < 0)
                return -1;
            else 
                return 0; // Desempate 8. Sorteo.
        }
    };

    private final static Comparator<Team> ascThirds = new Comparator<Team>(){
        public int compare(Team b1, Team b2){
            if(b1 != null && b2 == null)
                return 1;
            else if (b1 == null && b2 != null)
                return -1;
            else if (b1 == null && b2 == null)
                return 0;            
            
            if(b1.getPoints()-b2.getPoints()>0)
                return 1;
            else if(b1.getPoints()-b2.getPoints()<0)
                return -1;
            
            Match[] matchesA = getMatches(b1);
            Match[] matchesB = getMatches(b2);
            int goalDiffA = 0;
            int goalDiffB = 0;
            int goalSumA = 0;
            int goalSumB = 0;
            for(int i = 0; i < matchesA.length; i++) { 
                if(matchesA[i] != null) {
                    if(matchesA[i].getLocal().equals(b1)){
                        goalDiffA = Math.max(matchesA[i].getGoalsLocal()-matchesA[i].getGoalsVisitor(), goalDiffA);
                        goalSumA += matchesA[i].getGoalsLocal();
                    }
                    else {
                        goalDiffA = Math.max(matchesA[i].getGoalsVisitor()-matchesA[i].getGoalsLocal(), goalDiffA);
                        goalSumA += matchesA[i].getGoalsVisitor();
                    }
                }
            }
            for(int i = 0; i < matchesB.length; i++) { 
                if(matchesB[i] != null) {
                    if(matchesB[i].getLocal().equals(b2)){
                        goalDiffB = Math.max(matchesB[i].getGoalsVisitor()-matchesB[i].getGoalsLocal(), goalDiffB);
                        goalSumB += matchesB[i].getGoalsVisitor();
                    }
                    else {
                        goalDiffB = Math.max(matchesB[i].getGoalsLocal()-matchesB[i].getGoalsVisitor(), goalDiffB);
                        goalSumB += matchesB[i].getGoalsLocal();
                    }
                }
            }                        
            //Desempate 1. Mejor diferencia de gol en todos los partidos de grupo.
            if(goalDiffA - goalDiffB > 0)
                return 1;
            else if(goalDiffA - goalDiffB < 0)
                return -1;
            
            //Desempate 2. Mayor número de goles marcados en todos los partidos de grupo.
            if(goalSumA - goalSumB > 0)
                return 1;
            else if(goalSumA - goalSumB < 0)
                return -1;
            
            //Desempate 3. NO APLICA. La conducta Fair Play en todos los partidos de grupo (De acuerdo a lo definido en el Anexo C.5.1 del reglamento del torneo).
            //Desempate 4. Posición en el Ranking de coeficientes de la UEFA con que se inició este torneo.
            if(b1.getUefaCoefficient() - b2.getUefaCoefficient() > 0)
                return 1;
            else if(b1.getUefaCoefficient() - b2.getUefaCoefficient() < 0)
                return -1;
            else
                return 0; // Desempate 5. Sorteo.
        }
    };
    
    private void parseConfiguration(){
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("config.cfg"), "Cp1252"));
            String line = reader.readLine();
            int parsePhase = 0;
            while (line != null) {
                boolean control = false;
                line = line.trim();
                
                //Comments
                if (line.startsWith("#"))
                    control = true;
                else if(line.indexOf("#") > 0) 
                    line = line.substring(0, line.indexOf("#")).trim();
                
                if(line.equals("EQUIPOS:")) {
                    parsePhase = 0;
                    control = true;
                }
                else if(line.equals("PARTIDOS:")){
                    parsePhase = 1;
                    control = true;
                }
                
                if(!control && line.length() > 0){
                    if(parsePhase == 0){
                        String[] elements = line.split(",");
                        boolean teamOk = false;

                        Iterator it = teams.entrySet().iterator();
                        while(it.hasNext()){                    
                            Team t = (Team)((Map.Entry)it.next()).getValue();
                            if(t.getNameFull().equals(elements[0].trim())){
                                    t.setQuality(Double.parseDouble(elements[1].trim()));
                                    teamOk = true;
                                    //System.out.println(countries[i].getNameFull() + " - " + countries[i].getQuality());
                                }
                        }

                        if (!teamOk)
                            System.err.println("Problema configuracion de " + elements[0] + ".");
                    }
                    if(parsePhase == 1){
                        String[] elements = line.split(",");
                        int matchNumber = Integer.parseInt(elements[0].trim())-1;
                        matches[matchNumber].setFixed(true);
                        matches[matchNumber].setResult(Integer.parseInt(elements[1].trim()), Integer.parseInt(elements[2].trim()));   
                        updateComponentsMatch(matches[matchNumber], false); // TODO Falla al poner partidos fuera del grupo.
                    }
                }
                
                line = reader.readLine();
            }
            reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
    
    private void resetCountries(){    
        teams.put("Albania", new Team("Albania",1,      0.400, 23.216, "AL"));
        teams.put("Alemania", new Team("Alemania",3,     1.000, 40.236, "DE"));
        teams.put("Austria", new Team("Austria",6,      0.200, 30.932, "AT"));
        teams.put("Bélgica", new Team("Bélgica",5,      0.700, 34.442, "BE"));
        teams.put("Croacia", new Team("Croacia",4,      0.700, 30.642, "HR"));
        teams.put("Eslovaquia", new Team("Eslovaquia",2,   0.600, 27.131, "SK"));
        teams.put("España", new Team("España",4,       0.500, 37.962, "ES"));
        teams.put("Francia", new Team("Francia",1,      0.600, 33.599, "FR"));
        teams.put("Gales", new Team("Gales",2,        0.300, 24.521, "_wales"));
        teams.put("Hungría", new Team("Hungría",6,      0.500, 27.142, "HU"));
        teams.put("Inglaterra", new Team("Inglaterra",2,   0.900, 35.963, "_england"));
        teams.put("Irlanda N.", new Team("Irlanda N.",3,   0.100, 22.961, "_northern-ireland"));
        teams.put("Islandia", new Team("Islandia",6,     0.200, 25.388, "IS"));
        teams.put("Italia", new Team("Italia",5,       0.900, 34.345, "IT"));
        teams.put("Polonia", new Team("Polonia",3,      0.400, 28.306, "PL"));
        teams.put("Portugal", new Team("Portugal",6,     0.800, 35.138, "PT"));
        teams.put("Rep. Checa", new Team("Rep. Checa",4,   0.600, 29.403, "CZ"));
        teams.put("Rep. Irlanda", new Team("Rep. Irlanda",5, 0.600, 26.902, "IE"));
        teams.put("Rumanía", new Team("Rumanía",1,      0.500, 28.038, "RO"));
        teams.put("Rusia", new Team("Rusia",2,        0.700, 31.345, "RU"));
        teams.put("Suecia", new Team("Suecia",5,       0.700, 29.028, "SE"));
        teams.put("Suiza", new Team("Suiza",1,        0.600, 31.254, "CH"));
        teams.put("Turquía", new Team("Turquía",4,      0.600, 27.033, "TR"));
        teams.put("Ucrania", new Team("Ucrania",3,      0.700, 30.313, "UA"));
        
        // Load flags from .zip file.
        try {
            ZipFile zip = new ZipFile("flags.zip");
            Enumeration <? extends ZipEntry>e = zip.entries();

            while(e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();
                String entryName = entry.getName();
                int entrySize = (int)entry.getSize();

                if (entrySize > 0 && entryName.startsWith("flags-iso/shiny/24/")){
                    Iterator it = teams.entrySet().iterator();
                    while(it.hasNext()){
                        Team t = (Team)((Map.Entry)it.next()).getValue();
                    
                        if (t.getFlag() == null && entryName.endsWith(t.getCode()+".png")) {
                            //System.out.println(entryName);
                            byte[] buffer = new byte[entrySize];
                            zip.getInputStream(entry).read(buffer, 0, entrySize);
                            t.setFlag(new ImageIcon(ImageIO.read(new ByteArrayInputStream(buffer))));
                        }
                    }
                }
            }
            zip.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
         
    }
    
    private void resetMatches(){
        matches[0] = new Match(1, 1, teams.get("Francia"), teams.get("Rumanía"));
        matches[1] = new Match(1, 2, teams.get("Albania"), teams.get("Suiza"));
        matches[2] = new Match(1, 3, teams.get("Rumanía"), teams.get("Suiza"));
        matches[3] = new Match(1, 4, teams.get("Francia"), teams.get("Albania"));
        matches[4] = new Match(1, 5, teams.get("Rumanía"), teams.get("Albania"));
        matches[5] = new Match(1, 6, teams.get("Suiza"), teams.get("Francia"));

        matches[6] = new Match(2, 7, teams.get("Gales"), teams.get("Eslovaquia"));
        matches[7] = new Match(2, 8, teams.get("Inglaterra"), teams.get("Rusia"));
        matches[8] = new Match(2, 9, teams.get("Rusia"), teams.get("Eslovaquia"));
        matches[9] = new Match(2, 10, teams.get("Inglaterra"), teams.get("Gales"));
        matches[10] = new Match(2, 11, teams.get("Eslovaquia"), teams.get("Inglaterra"));
        matches[11] = new Match(2, 12, teams.get("Rusia"), teams.get("Gales"));
        
        matches[12] = new Match(3, 13, teams.get("Alemania"), teams.get("Ucrania"));
        matches[13] = new Match(3, 14, teams.get("Polonia"), teams.get("Irlanda N."));
        matches[14] = new Match(3, 15, teams.get("Ucrania"), teams.get("Irlanda N."));
        matches[15] = new Match(3, 16, teams.get("Alemania"), teams.get("Polonia"));
        matches[16] = new Match(3, 17, teams.get("Ucrania"), teams.get("Polonia"));
        matches[17] = new Match(3, 18, teams.get("Irlanda N."), teams.get("Alemania"));
        
        matches[18] = new Match(4, 19, teams.get("Turquía"), teams.get("Croacia"));
        matches[19] = new Match(4, 20, teams.get("España"), teams.get("Rep. Checa"));
        matches[20] = new Match(4, 21, teams.get("España"), teams.get("Turquía"));
        matches[21] = new Match(4, 22, teams.get("Rep. Checa"), teams.get("Croacia"));
        matches[22] = new Match(4, 23, teams.get("Croacia"), teams.get("España"));
        matches[23] = new Match(4, 24, teams.get("Rep. Checa"), teams.get("Turquía"));
        
        matches[24] = new Match(5, 25, teams.get("Bélgica"), teams.get("Italia"));
        matches[25] = new Match(5, 26, teams.get("Rep. Irlanda"), teams.get("Suecia"));
        matches[26] = new Match(5, 27, teams.get("Italia"), teams.get("Suecia"));
        matches[27] = new Match(5, 28, teams.get("Bélgica"), teams.get("Rep. Irlanda"));
        matches[28] = new Match(5, 29, teams.get("Italia"), teams.get("Rep. Irlanda"));
        matches[29] = new Match(5, 30, teams.get("Suecia"), teams.get("Bélgica"));
        
        matches[30] = new Match(6, 31, teams.get("Austria"), teams.get("Hungría"));
        matches[31] = new Match(6, 32, teams.get("Portugal"), teams.get("Islandia"));
        matches[32] = new Match(6, 33, teams.get("Islandia"), teams.get("Hungría"));
        matches[33] = new Match(6, 34, teams.get("Portugal"), teams.get("Austria"));
        matches[34] = new Match(6, 35, teams.get("Hungría"), teams.get("Portugal"));
        matches[35] = new Match(6, 36, teams.get("Islandia"), teams.get("Austria"));
        
        for(int i = 0; i < 36; i ++)
            updateComponentsMatch(matches[i], true);                        
    }
    
    /**
     * Creates new form PorraSimulator
     */
    public Simulator(){        
        initComponents();       
        setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this.setTitle("EURO 2016 - SAAC Simulator (" + version + ")");
        jButton1.requestFocus();        
        
        createComponentMap();
        resetCountries();
        resetMatches();
        parseConfiguration();
        
        /*
        // http://www.tutorialspoint.com/sqlite/sqlite_java.htm
        // http://openfootball.github.io/
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:worldcup.db");c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            //ResultSet rs = stmt.executeQuery( "SELECT * FROM games WHERE round_id IN (SELECT id FROM rounds WHERE event_id=10);" );
            //ResultSet rs = stmt.executeQuery( "SELECT team1_id,team2_id,score1,score2 FROM games WHERE round_id IN (SELECT id FROM rounds WHERE event_id=10);" );
            ResultSet rs = stmt.executeQuery(
                    "SELECT * " +
                    "FROM games G " +
                        "INNER JOIN teams T ON G.team1_id=T.id " +
                        "INNER JOIN teams U ON G.team2_id=U.id " +
                    "WHERE round_id IN (SELECT id FROM rounds WHERE event_id=10);" );
            
            while ( rs.next() ) {
                String id = rs.getString("title");
                String id2 = rs.getString("title");
                System.out.println(id + " - " + id2);
            }
            rs.close();
            stmt.close();
            c.close();
        }
        catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        */
    }
    
    private static Team[] getGroup(int group){
        int counter = 0;
        Team[] out = new Team[4];
        
        Iterator it = teams.entrySet().iterator();
        while (it.hasNext()) {
            Team t = (Team)((Map.Entry)it.next()).getValue();
            
            if(t.getGroupId() == group){
                out[counter] = t;
                counter++;
            }
        }
        return out;
    }    
    
    private static Team[] getThirds(){
        int counter = 0;
        Team[] out = new Team[6];
        
        Iterator it = teams.entrySet().iterator();
        while (it.hasNext()) {
            Team t = (Team)((Map.Entry)it.next()).getValue();
            
            if(t.getClassificationGroups()== 1){
                out[counter] = t;
                counter++;
            }
        }        
        return out;
    }    
    
    private static Match[] getMatches(Team team1, Team team2) {
        Match[] out = new Match[10];
        for(int i = 0, m = 0; i < matches.length && m < out.length; i++){
            if(matches[i] != null && matches[i].isPlayed()){
                if( matches[i].getLocal().equals(team1) && matches[i].getVisitor().equals(team2) ||
                    matches[i].getLocal().equals(team2) && matches[i].getVisitor().equals(team1)){

                    out[m] = matches[i];
                    m++;
                }
            }
        }
        return out;
    }
    
    private static Match[] getMatches(Team team) {
        Match[] out = new Match[10];
        for(int i = 0, m = 0; i < matches.length && m < out.length; i++){
            if(matches[i] != null && matches[i].isPlayed()){
                if(matches[i].getLocal().equals(team) || matches[i].getVisitor().equals(team)){
                    out[m] = matches[i];
                    m++;
                }
            }
        }
        return out;
    }
    
    // http://stackoverflow.com/a/7277323
    private void createComponentMap() {
        componentMap = new HashMap();
        Component[] components = group1.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);
                
        components = group2.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);
        
        components = group3.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);
        
        components = group4.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);
        
        components = group5.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);     
        
        components = group6.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);  
        
        components = playoffs.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);
        
        components = Left8s.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);
        
        components = Right8s.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);      
        
        components = Left4s.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);   
        
        components = Right4s.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);   
        
        components = CenterMatches.getComponents();
        for (int i=0; i < components.length; i++) 
            componentMap.put(components[i].getName(), components[i]);           
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        form = new javax.swing.JPanel();
        group1 = new javax.swing.JPanel();
        TitleGroup_1 = new javax.swing.JLabel();
        flagL1_1 = new javax.swing.JLabel();
        scoreL1_1 = new javax.swing.JTextField();
        separator1_1 = new javax.swing.JLabel();
        scoreV1_1 = new javax.swing.JTextField();
        flagV1_1 = new javax.swing.JLabel();
        flagL2_1 = new javax.swing.JLabel();
        scoreL2_1 = new javax.swing.JTextField();
        separator2_1 = new javax.swing.JLabel();
        scoreV2_1 = new javax.swing.JTextField();
        flagV2_1 = new javax.swing.JLabel();
        flagL3_1 = new javax.swing.JLabel();
        scoreL3_1 = new javax.swing.JTextField();
        separator3_1 = new javax.swing.JLabel();
        scoreV3_1 = new javax.swing.JTextField();
        flagV3_1 = new javax.swing.JLabel();
        flagL4_1 = new javax.swing.JLabel();
        scoreL4_1 = new javax.swing.JTextField();
        separator4_1 = new javax.swing.JLabel();
        scoreV4_1 = new javax.swing.JTextField();
        flagV4_1 = new javax.swing.JLabel();
        flagL5_1 = new javax.swing.JLabel();
        scoreL5_1 = new javax.swing.JTextField();
        separator5_1 = new javax.swing.JLabel();
        scoreV5_1 = new javax.swing.JTextField();
        flagV5_1 = new javax.swing.JLabel();
        flagL6_1 = new javax.swing.JLabel();
        scoreL6_1 = new javax.swing.JTextField();
        separator6_1 = new javax.swing.JLabel();
        scoreV6_1 = new javax.swing.JTextField();
        flagV6_1 = new javax.swing.JLabel();
        group2 = new javax.swing.JPanel();
        TitleGroup_2 = new javax.swing.JLabel();
        flagL1_2 = new javax.swing.JLabel();
        scoreL1_2 = new javax.swing.JTextField();
        separator1_2 = new javax.swing.JLabel();
        scoreV1_2 = new javax.swing.JTextField();
        flagV1_2 = new javax.swing.JLabel();
        flagL2_2 = new javax.swing.JLabel();
        scoreL2_2 = new javax.swing.JTextField();
        separator2_2 = new javax.swing.JLabel();
        scoreV2_2 = new javax.swing.JTextField();
        flagV2_2 = new javax.swing.JLabel();
        flagL3_2 = new javax.swing.JLabel();
        scoreL3_2 = new javax.swing.JTextField();
        separator3_2 = new javax.swing.JLabel();
        scoreV3_2 = new javax.swing.JTextField();
        flagV3_2 = new javax.swing.JLabel();
        flagL4_2 = new javax.swing.JLabel();
        scoreL4_2 = new javax.swing.JTextField();
        separator4_2 = new javax.swing.JLabel();
        scoreV4_2 = new javax.swing.JTextField();
        flagV4_2 = new javax.swing.JLabel();
        flagL5_2 = new javax.swing.JLabel();
        scoreL5_2 = new javax.swing.JTextField();
        separator5_2 = new javax.swing.JLabel();
        scoreV5_2 = new javax.swing.JTextField();
        flagV5_2 = new javax.swing.JLabel();
        flagL6_2 = new javax.swing.JLabel();
        scoreL6_2 = new javax.swing.JTextField();
        separator6_2 = new javax.swing.JLabel();
        scoreV6_2 = new javax.swing.JTextField();
        flagV6_2 = new javax.swing.JLabel();
        group3 = new javax.swing.JPanel();
        TitleGroup_3 = new javax.swing.JLabel();
        flagL1_3 = new javax.swing.JLabel();
        scoreL1_3 = new javax.swing.JTextField();
        separator1_3 = new javax.swing.JLabel();
        scoreV1_3 = new javax.swing.JTextField();
        flagV1_3 = new javax.swing.JLabel();
        flagL2_3 = new javax.swing.JLabel();
        scoreL2_3 = new javax.swing.JTextField();
        separator2_3 = new javax.swing.JLabel();
        scoreV2_3 = new javax.swing.JTextField();
        flagV2_3 = new javax.swing.JLabel();
        flagL3_3 = new javax.swing.JLabel();
        scoreL3_3 = new javax.swing.JTextField();
        separator3_3 = new javax.swing.JLabel();
        scoreV3_3 = new javax.swing.JTextField();
        flagV3_3 = new javax.swing.JLabel();
        flagL4_3 = new javax.swing.JLabel();
        scoreL4_3 = new javax.swing.JTextField();
        separator4_3 = new javax.swing.JLabel();
        scoreV4_3 = new javax.swing.JTextField();
        flagV4_3 = new javax.swing.JLabel();
        flagL5_3 = new javax.swing.JLabel();
        scoreL5_3 = new javax.swing.JTextField();
        separator5_3 = new javax.swing.JLabel();
        scoreV5_3 = new javax.swing.JTextField();
        flagV5_3 = new javax.swing.JLabel();
        flagL6_3 = new javax.swing.JLabel();
        scoreL6_3 = new javax.swing.JTextField();
        separator6_3 = new javax.swing.JLabel();
        scoreV6_3 = new javax.swing.JTextField();
        flagV6_3 = new javax.swing.JLabel();
        group4 = new javax.swing.JPanel();
        TitleGroup_4 = new javax.swing.JLabel();
        flagL1_4 = new javax.swing.JLabel();
        scoreL1_4 = new javax.swing.JTextField();
        separator1_4 = new javax.swing.JLabel();
        scoreV1_4 = new javax.swing.JTextField();
        flagV1_4 = new javax.swing.JLabel();
        flagL2_4 = new javax.swing.JLabel();
        scoreL2_4 = new javax.swing.JTextField();
        separator2_4 = new javax.swing.JLabel();
        scoreV2_4 = new javax.swing.JTextField();
        flagV2_4 = new javax.swing.JLabel();
        flagL3_4 = new javax.swing.JLabel();
        scoreL3_4 = new javax.swing.JTextField();
        separator3_4 = new javax.swing.JLabel();
        scoreV3_4 = new javax.swing.JTextField();
        flagV3_4 = new javax.swing.JLabel();
        flagL4_4 = new javax.swing.JLabel();
        scoreL4_4 = new javax.swing.JTextField();
        separator4_4 = new javax.swing.JLabel();
        scoreV4_4 = new javax.swing.JTextField();
        flagV4_4 = new javax.swing.JLabel();
        flagL5_4 = new javax.swing.JLabel();
        scoreL5_4 = new javax.swing.JTextField();
        separator5_4 = new javax.swing.JLabel();
        scoreV5_4 = new javax.swing.JTextField();
        flagV5_4 = new javax.swing.JLabel();
        flagL6_4 = new javax.swing.JLabel();
        scoreL6_4 = new javax.swing.JTextField();
        separator6_4 = new javax.swing.JLabel();
        scoreV6_4 = new javax.swing.JTextField();
        flagV6_4 = new javax.swing.JLabel();
        group5 = new javax.swing.JPanel();
        TitleGroup_5 = new javax.swing.JLabel();
        flagL1_5 = new javax.swing.JLabel();
        scoreL1_5 = new javax.swing.JTextField();
        separator1_5 = new javax.swing.JLabel();
        scoreV1_5 = new javax.swing.JTextField();
        flagV1_5 = new javax.swing.JLabel();
        flagL2_5 = new javax.swing.JLabel();
        scoreL2_5 = new javax.swing.JTextField();
        separator2_5 = new javax.swing.JLabel();
        scoreV2_5 = new javax.swing.JTextField();
        flagV2_5 = new javax.swing.JLabel();
        flagL3_5 = new javax.swing.JLabel();
        scoreL3_5 = new javax.swing.JTextField();
        separator3_5 = new javax.swing.JLabel();
        scoreV3_5 = new javax.swing.JTextField();
        flagV3_5 = new javax.swing.JLabel();
        flagL4_5 = new javax.swing.JLabel();
        scoreL4_5 = new javax.swing.JTextField();
        separator4_5 = new javax.swing.JLabel();
        scoreV4_5 = new javax.swing.JTextField();
        flagV4_5 = new javax.swing.JLabel();
        flagL5_5 = new javax.swing.JLabel();
        scoreL5_5 = new javax.swing.JTextField();
        separator5_5 = new javax.swing.JLabel();
        scoreV5_5 = new javax.swing.JTextField();
        flagV5_5 = new javax.swing.JLabel();
        flagL6_5 = new javax.swing.JLabel();
        scoreL6_5 = new javax.swing.JTextField();
        separator6_5 = new javax.swing.JLabel();
        scoreV6_5 = new javax.swing.JTextField();
        flagV6_5 = new javax.swing.JLabel();
        group6 = new javax.swing.JPanel();
        TitleGroup_6 = new javax.swing.JLabel();
        flagL1_6 = new javax.swing.JLabel();
        scoreL1_6 = new javax.swing.JTextField();
        separator1_6 = new javax.swing.JLabel();
        scoreV1_6 = new javax.swing.JTextField();
        flagV1_6 = new javax.swing.JLabel();
        flagL2_6 = new javax.swing.JLabel();
        scoreL2_6 = new javax.swing.JTextField();
        separator2_6 = new javax.swing.JLabel();
        scoreV2_6 = new javax.swing.JTextField();
        flagV2_6 = new javax.swing.JLabel();
        flagL3_6 = new javax.swing.JLabel();
        scoreL3_6 = new javax.swing.JTextField();
        separator3_6 = new javax.swing.JLabel();
        scoreV3_6 = new javax.swing.JTextField();
        flagV3_6 = new javax.swing.JLabel();
        flagL4_6 = new javax.swing.JLabel();
        scoreL4_6 = new javax.swing.JTextField();
        separator4_6 = new javax.swing.JLabel();
        scoreV4_6 = new javax.swing.JTextField();
        flagV4_6 = new javax.swing.JLabel();
        flagL5_6 = new javax.swing.JLabel();
        scoreL5_6 = new javax.swing.JTextField();
        separator5_6 = new javax.swing.JLabel();
        scoreV5_6 = new javax.swing.JTextField();
        flagV5_6 = new javax.swing.JLabel();
        flagL6_6 = new javax.swing.JLabel();
        scoreL6_6 = new javax.swing.JTextField();
        separator6_6 = new javax.swing.JLabel();
        scoreV6_6 = new javax.swing.JTextField();
        flagV6_6 = new javax.swing.JLabel();
        playoffs = new javax.swing.JPanel();
        PlayoffTitles = new javax.swing.JPanel();
        TitleGroup_7 = new javax.swing.JLabel();
        TitleGroup_8 = new javax.swing.JLabel();
        TitleGroup_9 = new javax.swing.JLabel();
        TitleGroup_11 = new javax.swing.JLabel();
        TitleGroup_12 = new javax.swing.JLabel();
        TitleGroup_13 = new javax.swing.JLabel();
        Left8s = new javax.swing.JPanel();
        flag8s_2_1 = new javax.swing.JLabel();
        scoreL1_7 = new javax.swing.JTextField();
        separator1_7 = new javax.swing.JLabel();
        scoreV1_7 = new javax.swing.JTextField();
        flag8s_2_3 = new javax.swing.JLabel();
        flag8s_3_2 = new javax.swing.JLabel();
        scoreV1_9 = new javax.swing.JTextField();
        flag8s_3_1 = new javax.swing.JLabel();
        scoreV1_8 = new javax.swing.JTextField();
        separator1_8 = new javax.swing.JLabel();
        scoreL1_8 = new javax.swing.JTextField();
        flag8s_1_4 = new javax.swing.JLabel();
        flag8s_1_2 = new javax.swing.JLabel();
        scoreL1_9 = new javax.swing.JTextField();
        separator1_9 = new javax.swing.JLabel();
        scoreV1_10 = new javax.swing.JTextField();
        flag8s_2_5 = new javax.swing.JLabel();
        separator1_10 = new javax.swing.JLabel();
        scoreL1_10 = new javax.swing.JTextField();
        flag8s_1_6 = new javax.swing.JLabel();
        Left4s = new javax.swing.JPanel();
        scoreL1_11 = new javax.swing.JTextField();
        flagL1_11 = new javax.swing.JLabel();
        separator1_11 = new javax.swing.JLabel();
        scoreV1_11 = new javax.swing.JTextField();
        flagV1_11 = new javax.swing.JLabel();
        flagV1_12 = new javax.swing.JLabel();
        scoreV1_12 = new javax.swing.JTextField();
        separator1_12 = new javax.swing.JLabel();
        scoreL1_12 = new javax.swing.JTextField();
        flagL1_12 = new javax.swing.JLabel();
        CenterMatches = new javax.swing.JPanel();
        flagL1_13 = new javax.swing.JLabel();
        scoreL1_13 = new javax.swing.JTextField();
        separator1_13 = new javax.swing.JLabel();
        scoreV1_13 = new javax.swing.JTextField();
        flagV1_13 = new javax.swing.JLabel();
        flagL1_14 = new javax.swing.JLabel();
        scoreL1_14 = new javax.swing.JTextField();
        separator1_14 = new javax.swing.JLabel();
        scoreV1_14 = new javax.swing.JTextField();
        flagV1_14 = new javax.swing.JLabel();
        flagV1_15 = new javax.swing.JLabel();
        scoreV1_15 = new javax.swing.JTextField();
        separator1_15 = new javax.swing.JLabel();
        scoreL1_15 = new javax.swing.JTextField();
        flagL1_15 = new javax.swing.JLabel();
        TitleGroup_10 = new javax.swing.JLabel();
        Right4s = new javax.swing.JPanel();
        flagL1_17 = new javax.swing.JLabel();
        scoreL1_17 = new javax.swing.JTextField();
        separator1_17 = new javax.swing.JLabel();
        scoreV1_17 = new javax.swing.JTextField();
        flagV1_17 = new javax.swing.JLabel();
        flagV1_16 = new javax.swing.JLabel();
        scoreV1_16 = new javax.swing.JTextField();
        separator1_16 = new javax.swing.JLabel();
        scoreL1_16 = new javax.swing.JTextField();
        flagL1_16 = new javax.swing.JLabel();
        Right8s = new javax.swing.JPanel();
        scoreV1_21 = new javax.swing.JTextField();
        scoreV1_20 = new javax.swing.JTextField();
        separator1_20 = new javax.swing.JLabel();
        flag8s_1_5 = new javax.swing.JLabel();
        scoreL1_21 = new javax.swing.JTextField();
        flag8s_3_3 = new javax.swing.JLabel();
        scoreV1_18 = new javax.swing.JTextField();
        flag8s_2_6 = new javax.swing.JLabel();
        scoreL1_18 = new javax.swing.JTextField();
        scoreL1_20 = new javax.swing.JTextField();
        flag8s_2_4 = new javax.swing.JLabel();
        flag8s_2_2 = new javax.swing.JLabel();
        flag8s_1_1 = new javax.swing.JLabel();
        separator1_21 = new javax.swing.JLabel();
        separator1_18 = new javax.swing.JLabel();
        separator1_19 = new javax.swing.JLabel();
        scoreL1_19 = new javax.swing.JTextField();
        scoreV1_19 = new javax.swing.JTextField();
        flag8s_1_3 = new javax.swing.JLabel();
        flag8s_3_4 = new javax.swing.JLabel();
        flagWinner = new javax.swing.JLabel();
        WinnerTitle = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        group1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TitleGroup_1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_1.setText("Grupo A");
        TitleGroup_1.setFocusCycleRoot(true);
        TitleGroup_1.setFocusable(false);

        flagL1_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_1.setName("flag_L_1"); // NOI18N

        scoreL1_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_1.setName("score_L_1"); // NOI18N
        scoreL1_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_1.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_1.setText("-");
        separator1_1.setName("separator_1"); // NOI18N
        separator1_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_1.setName("score_V_1"); // NOI18N
        scoreV1_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_1.setName("flag_V_1"); // NOI18N

        flagL2_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL2_1.setToolTipText("");
        flagL2_1.setName("flag_L_2"); // NOI18N
        flagL2_1.setNextFocusableComponent(flagL1_1);

        scoreL2_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL2_1.setName("score_L_2"); // NOI18N
        scoreL2_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator2_1.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator2_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator2_1.setText("-");
        separator2_1.setName("separator_2"); // NOI18N
        separator2_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV2_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV2_1.setName("score_V_2"); // NOI18N
        scoreV2_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV2_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV2_1.setName("flag_V_2"); // NOI18N

        flagL3_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL3_1.setToolTipText("");
        flagL3_1.setName("flag_L_3"); // NOI18N
        flagL3_1.setNextFocusableComponent(flagL1_1);

        scoreL3_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL3_1.setName("score_L_3"); // NOI18N
        scoreL3_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator3_1.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator3_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator3_1.setText("-");
        separator3_1.setName("separator_3"); // NOI18N
        separator3_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV3_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV3_1.setName("score_V_3"); // NOI18N
        scoreV3_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV3_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV3_1.setName("flag_V_3"); // NOI18N

        flagL4_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL4_1.setToolTipText("");
        flagL4_1.setName("flag_L_4"); // NOI18N
        flagL4_1.setNextFocusableComponent(flagL1_1);

        scoreL4_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL4_1.setName("score_L_4"); // NOI18N
        scoreL4_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator4_1.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator4_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator4_1.setText("-");
        separator4_1.setName("separator_4"); // NOI18N
        separator4_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV4_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV4_1.setName("score_V_4"); // NOI18N
        scoreV4_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV4_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV4_1.setName("flag_V_4"); // NOI18N

        flagL5_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL5_1.setToolTipText("");
        flagL5_1.setName("flag_L_5"); // NOI18N
        flagL5_1.setNextFocusableComponent(flagL1_1);

        scoreL5_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL5_1.setName("score_L_5"); // NOI18N
        scoreL5_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator5_1.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator5_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator5_1.setText("-");
        separator5_1.setName("separator_5"); // NOI18N
        separator5_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV5_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV5_1.setName("score_V_5"); // NOI18N
        scoreV5_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV5_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV5_1.setName("flag_V_5"); // NOI18N

        flagL6_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL6_1.setToolTipText("");
        flagL6_1.setName("flag_L_6"); // NOI18N
        flagL6_1.setNextFocusableComponent(flagL1_1);

        scoreL6_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL6_1.setName("score_L_6"); // NOI18N
        scoreL6_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator6_1.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator6_1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator6_1.setText("-");
        separator6_1.setName("separator_6"); // NOI18N
        separator6_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV6_1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV6_1.setName("score_V_6"); // NOI18N
        scoreV6_1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV6_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV6_1.setName("flag_V_6"); // NOI18N

        javax.swing.GroupLayout group1Layout = new javax.swing.GroupLayout(group1);
        group1.setLayout(group1Layout);
        group1Layout.setHorizontalGroup(
            group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(group1Layout.createSequentialGroup()
                            .addComponent(flagL5_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(scoreL5_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator5_1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV5_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flagV5_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(group1Layout.createSequentialGroup()
                                .addComponent(flagL1_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL1_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator1_1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV1_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV1_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group1Layout.createSequentialGroup()
                                .addComponent(flagL2_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL2_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator2_1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV2_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV2_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group1Layout.createSequentialGroup()
                                .addComponent(flagL3_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL3_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator3_1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV3_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV3_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group1Layout.createSequentialGroup()
                                .addComponent(flagL4_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL4_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator4_1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV4_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV4_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group1Layout.createSequentialGroup()
                                .addComponent(flagL6_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL6_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator6_1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV6_1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV6_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(TitleGroup_1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        group1Layout.setVerticalGroup(
            group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(group1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(TitleGroup_1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_1)
                        .addComponent(separator1_1)
                        .addComponent(scoreV1_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL2_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV2_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL2_1)
                        .addComponent(separator2_1)
                        .addComponent(scoreV2_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL3_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV3_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL3_1)
                        .addComponent(separator3_1)
                        .addComponent(scoreV3_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL4_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV4_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL4_1)
                        .addComponent(separator4_1)
                        .addComponent(scoreV4_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagV5_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL5_1)
                            .addComponent(separator5_1)
                            .addComponent(scoreV5_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(flagL5_1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL6_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV6_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL6_1)
                        .addComponent(separator6_1)
                        .addComponent(scoreV6_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        group2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TitleGroup_2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_2.setText("Grupo B");
        TitleGroup_2.setFocusCycleRoot(true);
        TitleGroup_2.setFocusable(false);

        flagL1_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_2.setToolTipText("");
        flagL1_2.setName("flag_L_7"); // NOI18N
        flagL1_2.setNextFocusableComponent(flagL1_1);

        scoreL1_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_2.setName("score_L_7"); // NOI18N
        scoreL1_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_2.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_2.setText("-");
        separator1_2.setName("separator_7"); // NOI18N
        separator1_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_2.setName("score_V_7"); // NOI18N
        scoreV1_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_2.setName("flag_V_7"); // NOI18N

        flagL2_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL2_2.setToolTipText("");
        flagL2_2.setName("flag_L_8"); // NOI18N
        flagL2_2.setNextFocusableComponent(flagL1_1);

        scoreL2_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL2_2.setName("score_L_8"); // NOI18N
        scoreL2_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator2_2.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator2_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator2_2.setText("-");
        separator2_2.setName("separator_8"); // NOI18N
        separator2_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV2_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV2_2.setName("score_V_8"); // NOI18N
        scoreV2_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV2_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV2_2.setName("flag_V_8"); // NOI18N

        flagL3_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL3_2.setToolTipText("");
        flagL3_2.setName("flag_L_9"); // NOI18N
        flagL3_2.setNextFocusableComponent(flagL1_1);

        scoreL3_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL3_2.setName("score_L_9"); // NOI18N
        scoreL3_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator3_2.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator3_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator3_2.setText("-");
        separator3_2.setName("separator_9"); // NOI18N
        separator3_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV3_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV3_2.setName("score_V_9"); // NOI18N
        scoreV3_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV3_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV3_2.setName("flag_V_9"); // NOI18N

        flagL4_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL4_2.setToolTipText("");
        flagL4_2.setName("flag_L_10"); // NOI18N
        flagL4_2.setNextFocusableComponent(flagL1_1);

        scoreL4_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL4_2.setName("score_L_10"); // NOI18N
        scoreL4_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator4_2.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator4_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator4_2.setText("-");
        separator4_2.setName("separator_10"); // NOI18N
        separator4_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV4_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV4_2.setName("score_V_10"); // NOI18N
        scoreV4_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV4_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV4_2.setName("flag_V_10"); // NOI18N

        flagL5_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL5_2.setToolTipText("");
        flagL5_2.setName("flag_L_11"); // NOI18N
        flagL5_2.setNextFocusableComponent(flagL1_1);

        scoreL5_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL5_2.setName("score_L_11"); // NOI18N
        scoreL5_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator5_2.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator5_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator5_2.setText("-");
        separator5_2.setName("separator_11"); // NOI18N
        separator5_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV5_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV5_2.setName("score_V_11"); // NOI18N
        scoreV5_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV5_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV5_2.setName("flag_V_11"); // NOI18N

        flagL6_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL6_2.setToolTipText("");
        flagL6_2.setName("flag_L_12"); // NOI18N
        flagL6_2.setNextFocusableComponent(flagL1_1);

        scoreL6_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL6_2.setName("score_L_12"); // NOI18N
        scoreL6_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator6_2.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator6_2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator6_2.setText("-");
        separator6_2.setName("separator_12"); // NOI18N
        separator6_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV6_2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV6_2.setName("score_V_12"); // NOI18N
        scoreV6_2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV6_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV6_2.setName("flag_V_12"); // NOI18N

        javax.swing.GroupLayout group2Layout = new javax.swing.GroupLayout(group2);
        group2.setLayout(group2Layout);
        group2Layout.setHorizontalGroup(
            group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(group2Layout.createSequentialGroup()
                            .addComponent(flagL5_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(scoreL5_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator5_2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV5_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flagV5_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(group2Layout.createSequentialGroup()
                                .addComponent(flagL1_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL1_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator1_2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV1_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV1_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group2Layout.createSequentialGroup()
                                .addComponent(flagL2_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL2_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator2_2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV2_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV2_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group2Layout.createSequentialGroup()
                                .addComponent(flagL3_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL3_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator3_2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV3_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV3_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group2Layout.createSequentialGroup()
                                .addComponent(flagL4_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL4_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator4_2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV4_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV4_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group2Layout.createSequentialGroup()
                                .addComponent(flagL6_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL6_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator6_2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV6_2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV6_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(TitleGroup_2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        group2Layout.setVerticalGroup(
            group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(group2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(TitleGroup_2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_2)
                        .addComponent(separator1_2)
                        .addComponent(scoreV1_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL2_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV2_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL2_2)
                        .addComponent(separator2_2)
                        .addComponent(scoreV2_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL3_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV3_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL3_2)
                        .addComponent(separator3_2)
                        .addComponent(scoreV3_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL4_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV4_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL4_2)
                        .addComponent(separator4_2)
                        .addComponent(scoreV4_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagV5_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL5_2)
                            .addComponent(separator5_2)
                            .addComponent(scoreV5_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(flagL5_2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL6_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV6_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL6_2)
                        .addComponent(separator6_2)
                        .addComponent(scoreV6_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        group3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TitleGroup_3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_3.setText("Grupo C");
        TitleGroup_3.setFocusCycleRoot(true);
        TitleGroup_3.setFocusable(false);

        flagL1_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_3.setToolTipText("");
        flagL1_3.setName("flag_L_13"); // NOI18N
        flagL1_3.setNextFocusableComponent(flagL1_1);

        scoreL1_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_3.setName("score_L_13"); // NOI18N
        scoreL1_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_3.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_3.setText("-");
        separator1_3.setName("separator_13"); // NOI18N
        separator1_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_3.setName("score_V_13"); // NOI18N
        scoreV1_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_3.setName("flag_V_13"); // NOI18N

        flagL2_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL2_3.setToolTipText("");
        flagL2_3.setName("flag_L_14"); // NOI18N
        flagL2_3.setNextFocusableComponent(flagL1_1);

        scoreL2_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL2_3.setName("score_L_14"); // NOI18N
        scoreL2_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator2_3.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator2_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator2_3.setText("-");
        separator2_3.setName("separator_14"); // NOI18N
        separator2_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV2_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV2_3.setName("score_V_14"); // NOI18N
        scoreV2_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV2_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV2_3.setName("flag_V_14"); // NOI18N

        flagL3_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL3_3.setToolTipText("");
        flagL3_3.setName("flag_L_15"); // NOI18N
        flagL3_3.setNextFocusableComponent(flagL1_1);

        scoreL3_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL3_3.setName("score_L_15"); // NOI18N
        scoreL3_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator3_3.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator3_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator3_3.setText("-");
        separator3_3.setName("separator_15"); // NOI18N
        separator3_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV3_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV3_3.setName("score_V_15"); // NOI18N
        scoreV3_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV3_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV3_3.setName("flag_V_15"); // NOI18N

        flagL4_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL4_3.setToolTipText("");
        flagL4_3.setName("flag_L_16"); // NOI18N
        flagL4_3.setNextFocusableComponent(flagL1_1);

        scoreL4_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL4_3.setName("score_L_16"); // NOI18N
        scoreL4_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator4_3.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator4_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator4_3.setText("-");
        separator4_3.setName("separator_16"); // NOI18N
        separator4_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV4_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV4_3.setName("score_V_16"); // NOI18N
        scoreV4_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV4_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV4_3.setName("flag_V_16"); // NOI18N

        flagL5_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL5_3.setToolTipText("");
        flagL5_3.setName("flag_L_17"); // NOI18N
        flagL5_3.setNextFocusableComponent(flagL1_1);

        scoreL5_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL5_3.setName("score_L_17"); // NOI18N
        scoreL5_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator5_3.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator5_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator5_3.setText("-");
        separator5_3.setName("separator_17"); // NOI18N
        separator5_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV5_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV5_3.setName("score_V_17"); // NOI18N
        scoreV5_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV5_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV5_3.setName("flag_V_17"); // NOI18N

        flagL6_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL6_3.setToolTipText("");
        flagL6_3.setName("flag_L_18"); // NOI18N
        flagL6_3.setNextFocusableComponent(flagL1_1);

        scoreL6_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL6_3.setName("score_L_18"); // NOI18N
        scoreL6_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator6_3.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator6_3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator6_3.setText("-");
        separator6_3.setName("separator_18"); // NOI18N
        separator6_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV6_3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV6_3.setName("score_V_18"); // NOI18N
        scoreV6_3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV6_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV6_3.setName("flag_V_18"); // NOI18N

        javax.swing.GroupLayout group3Layout = new javax.swing.GroupLayout(group3);
        group3.setLayout(group3Layout);
        group3Layout.setHorizontalGroup(
            group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(group3Layout.createSequentialGroup()
                            .addComponent(flagL5_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(scoreL5_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator5_3, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV5_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flagV5_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(group3Layout.createSequentialGroup()
                                .addComponent(flagL1_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL1_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator1_3, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV1_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV1_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group3Layout.createSequentialGroup()
                                .addComponent(flagL2_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL2_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator2_3, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV2_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV2_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group3Layout.createSequentialGroup()
                                .addComponent(flagL3_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL3_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator3_3, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV3_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV3_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group3Layout.createSequentialGroup()
                                .addComponent(flagL4_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL4_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator4_3, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV4_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV4_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group3Layout.createSequentialGroup()
                                .addComponent(flagL6_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL6_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator6_3, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV6_3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV6_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(TitleGroup_3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        group3Layout.setVerticalGroup(
            group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(group3Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(TitleGroup_3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_3)
                        .addComponent(separator1_3)
                        .addComponent(scoreV1_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL2_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV2_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL2_3)
                        .addComponent(separator2_3)
                        .addComponent(scoreV2_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL3_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV3_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL3_3)
                        .addComponent(separator3_3)
                        .addComponent(scoreV3_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL4_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV4_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL4_3)
                        .addComponent(separator4_3)
                        .addComponent(scoreV4_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagV5_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL5_3)
                            .addComponent(separator5_3)
                            .addComponent(scoreV5_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(flagL5_3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL6_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV6_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL6_3)
                        .addComponent(separator6_3)
                        .addComponent(scoreV6_3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        group4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TitleGroup_4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_4.setText("Grupo D");
        TitleGroup_4.setFocusCycleRoot(true);
        TitleGroup_4.setFocusable(false);

        flagL1_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_4.setToolTipText("");
        flagL1_4.setName("flag_L_19"); // NOI18N
        flagL1_4.setNextFocusableComponent(flagL1_1);

        scoreL1_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_4.setName("score_L_19"); // NOI18N
        scoreL1_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_4.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_4.setText("-");
        separator1_4.setName("separator_19"); // NOI18N
        separator1_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_4.setName("score_V_19"); // NOI18N
        scoreV1_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_4.setName("flag_V_19"); // NOI18N

        flagL2_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL2_4.setToolTipText("");
        flagL2_4.setName("flag_L_20"); // NOI18N
        flagL2_4.setNextFocusableComponent(flagL1_1);

        scoreL2_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL2_4.setName("score_L_20"); // NOI18N
        scoreL2_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator2_4.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator2_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator2_4.setText("-");
        separator2_4.setName("separator_20"); // NOI18N
        separator2_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV2_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV2_4.setName("score_V_20"); // NOI18N
        scoreV2_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV2_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV2_4.setName("flag_V_20"); // NOI18N

        flagL3_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL3_4.setToolTipText("");
        flagL3_4.setName("flag_L_21"); // NOI18N
        flagL3_4.setNextFocusableComponent(flagL1_1);

        scoreL3_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL3_4.setName("score_L_21"); // NOI18N
        scoreL3_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator3_4.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator3_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator3_4.setText("-");
        separator3_4.setName("separator_21"); // NOI18N
        separator3_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV3_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV3_4.setName("score_V_21"); // NOI18N
        scoreV3_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV3_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV3_4.setName("flag_V_21"); // NOI18N

        flagL4_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL4_4.setToolTipText("");
        flagL4_4.setName("flag_L_22"); // NOI18N
        flagL4_4.setNextFocusableComponent(flagL1_1);

        scoreL4_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL4_4.setName("score_L_22"); // NOI18N
        scoreL4_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator4_4.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator4_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator4_4.setText("-");
        separator4_4.setName("separator_22"); // NOI18N
        separator4_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV4_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV4_4.setName("score_V_22"); // NOI18N
        scoreV4_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV4_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV4_4.setName("flag_V_22"); // NOI18N

        flagL5_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL5_4.setToolTipText("");
        flagL5_4.setName("flag_L_23"); // NOI18N
        flagL5_4.setNextFocusableComponent(flagL1_1);

        scoreL5_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL5_4.setName("score_L_23"); // NOI18N
        scoreL5_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator5_4.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator5_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator5_4.setText("-");
        separator5_4.setName("separator_23"); // NOI18N
        separator5_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV5_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV5_4.setName("score_V_23"); // NOI18N
        scoreV5_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV5_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV5_4.setName("flag_V_23"); // NOI18N

        flagL6_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL6_4.setToolTipText("");
        flagL6_4.setName("flag_L_24"); // NOI18N
        flagL6_4.setNextFocusableComponent(flagL1_1);

        scoreL6_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL6_4.setName("score_L_24"); // NOI18N
        scoreL6_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator6_4.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator6_4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator6_4.setText("-");
        separator6_4.setName("separator_24"); // NOI18N
        separator6_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV6_4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV6_4.setName("score_V_24"); // NOI18N
        scoreV6_4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV6_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV6_4.setName("flag_V_24"); // NOI18N

        javax.swing.GroupLayout group4Layout = new javax.swing.GroupLayout(group4);
        group4.setLayout(group4Layout);
        group4Layout.setHorizontalGroup(
            group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(group4Layout.createSequentialGroup()
                            .addComponent(flagL5_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(scoreL5_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator5_4, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV5_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flagV5_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(group4Layout.createSequentialGroup()
                                .addComponent(flagL1_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL1_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator1_4, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV1_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV1_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group4Layout.createSequentialGroup()
                                .addComponent(flagL2_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL2_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator2_4, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV2_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV2_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group4Layout.createSequentialGroup()
                                .addComponent(flagL3_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL3_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator3_4, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV3_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV3_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group4Layout.createSequentialGroup()
                                .addComponent(flagL4_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL4_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator4_4, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV4_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV4_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group4Layout.createSequentialGroup()
                                .addComponent(flagL6_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL6_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator6_4, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV6_4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV6_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(TitleGroup_4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        group4Layout.setVerticalGroup(
            group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(group4Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(TitleGroup_4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_4)
                        .addComponent(separator1_4)
                        .addComponent(scoreV1_4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL2_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV2_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL2_4)
                        .addComponent(separator2_4)
                        .addComponent(scoreV2_4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL3_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV3_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL3_4)
                        .addComponent(separator3_4)
                        .addComponent(scoreV3_4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL4_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV4_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL4_4)
                        .addComponent(separator4_4)
                        .addComponent(scoreV4_4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagV5_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL5_4)
                            .addComponent(separator5_4)
                            .addComponent(scoreV5_4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(flagL5_4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL6_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV6_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL6_4)
                        .addComponent(separator6_4)
                        .addComponent(scoreV6_4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        group5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TitleGroup_5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_5.setText("Grupo E");
        TitleGroup_5.setFocusCycleRoot(true);
        TitleGroup_5.setFocusable(false);

        flagL1_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_5.setToolTipText("");
        flagL1_5.setName("flag_L_25"); // NOI18N
        flagL1_5.setNextFocusableComponent(flagL1_1);

        scoreL1_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_5.setName("score_L_25"); // NOI18N
        scoreL1_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_5.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_5.setText("-");
        separator1_5.setName("separator_25"); // NOI18N
        separator1_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_5.setName("score_V_25"); // NOI18N
        scoreV1_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_5.setName("flag_V_25"); // NOI18N

        flagL2_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL2_5.setToolTipText("");
        flagL2_5.setName("flag_L_26"); // NOI18N
        flagL2_5.setNextFocusableComponent(flagL1_1);

        scoreL2_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL2_5.setName("score_L_26"); // NOI18N
        scoreL2_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator2_5.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator2_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator2_5.setText("-");
        separator2_5.setName("separator_26"); // NOI18N
        separator2_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV2_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV2_5.setName("score_V_26"); // NOI18N
        scoreV2_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV2_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV2_5.setName("flag_V_26"); // NOI18N

        flagL3_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL3_5.setToolTipText("");
        flagL3_5.setName("flag_L_27"); // NOI18N
        flagL3_5.setNextFocusableComponent(flagL1_1);

        scoreL3_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL3_5.setName("score_L_27"); // NOI18N
        scoreL3_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator3_5.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator3_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator3_5.setText("-");
        separator3_5.setName("separator_27"); // NOI18N
        separator3_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV3_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV3_5.setName("score_V_27"); // NOI18N
        scoreV3_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV3_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV3_5.setName("flag_V_27"); // NOI18N

        flagL4_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL4_5.setToolTipText("");
        flagL4_5.setName("flag_L_28"); // NOI18N
        flagL4_5.setNextFocusableComponent(flagL1_1);

        scoreL4_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL4_5.setName("score_L_28"); // NOI18N
        scoreL4_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator4_5.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator4_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator4_5.setText("-");
        separator4_5.setName("separator_28"); // NOI18N
        separator4_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV4_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV4_5.setName("score_V_28"); // NOI18N
        scoreV4_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV4_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV4_5.setName("flag_V_28"); // NOI18N

        flagL5_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL5_5.setToolTipText("");
        flagL5_5.setName("flag_L_29"); // NOI18N
        flagL5_5.setNextFocusableComponent(flagL1_1);

        scoreL5_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL5_5.setName("score_L_29"); // NOI18N
        scoreL5_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator5_5.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator5_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator5_5.setText("-");
        separator5_5.setName("separator_29"); // NOI18N
        separator5_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV5_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV5_5.setName("score_V_29"); // NOI18N
        scoreV5_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV5_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV5_5.setName("flag_V_29"); // NOI18N

        flagL6_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL6_5.setToolTipText("");
        flagL6_5.setName("flag_L_30"); // NOI18N
        flagL6_5.setNextFocusableComponent(flagL1_1);

        scoreL6_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL6_5.setName("score_L_30"); // NOI18N
        scoreL6_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator6_5.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator6_5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator6_5.setText("-");
        separator6_5.setName("separator_30"); // NOI18N
        separator6_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV6_5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV6_5.setName("score_V_30"); // NOI18N
        scoreV6_5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV6_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV6_5.setName("flag_V_30"); // NOI18N

        javax.swing.GroupLayout group5Layout = new javax.swing.GroupLayout(group5);
        group5.setLayout(group5Layout);
        group5Layout.setHorizontalGroup(
            group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(group5Layout.createSequentialGroup()
                            .addComponent(flagL5_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(scoreL5_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator5_5, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV5_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flagV5_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(group5Layout.createSequentialGroup()
                                .addComponent(flagL1_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL1_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator1_5, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV1_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV1_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group5Layout.createSequentialGroup()
                                .addComponent(flagL2_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL2_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator2_5, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV2_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV2_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group5Layout.createSequentialGroup()
                                .addComponent(flagL3_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL3_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator3_5, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV3_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV3_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group5Layout.createSequentialGroup()
                                .addComponent(flagL4_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL4_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator4_5, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV4_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV4_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group5Layout.createSequentialGroup()
                                .addComponent(flagL6_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL6_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator6_5, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV6_5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV6_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(TitleGroup_5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        group5Layout.setVerticalGroup(
            group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(group5Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(TitleGroup_5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_5)
                        .addComponent(separator1_5)
                        .addComponent(scoreV1_5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL2_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV2_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL2_5)
                        .addComponent(separator2_5)
                        .addComponent(scoreV2_5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL3_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV3_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL3_5)
                        .addComponent(separator3_5)
                        .addComponent(scoreV3_5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL4_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV4_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL4_5)
                        .addComponent(separator4_5)
                        .addComponent(scoreV4_5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagV5_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL5_5)
                            .addComponent(separator5_5)
                            .addComponent(scoreV5_5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(flagL5_5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL6_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV6_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL6_5)
                        .addComponent(separator6_5)
                        .addComponent(scoreV6_5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        group6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TitleGroup_6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_6.setText("Grupo F");
        TitleGroup_6.setFocusCycleRoot(true);
        TitleGroup_6.setFocusable(false);

        flagL1_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_6.setToolTipText("");
        flagL1_6.setName("flag_L_31"); // NOI18N
        flagL1_6.setNextFocusableComponent(flagL1_1);

        scoreL1_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_6.setName("score_L_31"); // NOI18N
        scoreL1_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_6.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_6.setText("-");
        separator1_6.setName("separator_31"); // NOI18N
        separator1_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_6.setName("score_V_31"); // NOI18N
        scoreV1_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_6.setName("flag_V_31"); // NOI18N

        flagL2_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL2_6.setToolTipText("");
        flagL2_6.setName("flag_L_32"); // NOI18N
        flagL2_6.setNextFocusableComponent(flagL1_1);

        scoreL2_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL2_6.setName("score_L_32"); // NOI18N
        scoreL2_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator2_6.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator2_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator2_6.setText("-");
        separator2_6.setName("separator_32"); // NOI18N
        separator2_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV2_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV2_6.setName("score_V_32"); // NOI18N
        scoreV2_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV2_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV2_6.setName("flag_V_32"); // NOI18N

        flagL3_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL3_6.setToolTipText("");
        flagL3_6.setName("flag_L_33"); // NOI18N
        flagL3_6.setNextFocusableComponent(flagL1_1);

        scoreL3_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL3_6.setName("score_L_33"); // NOI18N
        scoreL3_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator3_6.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator3_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator3_6.setText("-");
        separator3_6.setName("separator_33"); // NOI18N
        separator3_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV3_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV3_6.setName("score_V_33"); // NOI18N
        scoreV3_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV3_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV3_6.setName("flag_V_33"); // NOI18N

        flagL4_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL4_6.setToolTipText("");
        flagL4_6.setName("flag_L_34"); // NOI18N
        flagL4_6.setNextFocusableComponent(flagL1_1);

        scoreL4_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL4_6.setName("score_L_34"); // NOI18N
        scoreL4_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator4_6.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator4_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator4_6.setText("-");
        separator4_6.setName("separator_34"); // NOI18N
        separator4_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV4_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV4_6.setName("score_V_34"); // NOI18N
        scoreV4_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV4_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV4_6.setName("flag_V_34"); // NOI18N

        flagL5_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL5_6.setToolTipText("");
        flagL5_6.setName("flag_L_35"); // NOI18N
        flagL5_6.setNextFocusableComponent(flagL1_1);

        scoreL5_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL5_6.setName("score_L_35"); // NOI18N
        scoreL5_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator5_6.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator5_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator5_6.setText("-");
        separator5_6.setName("separator_35"); // NOI18N
        separator5_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV5_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV5_6.setName("score_V_35"); // NOI18N
        scoreV5_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV5_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV5_6.setName("flag_V_35"); // NOI18N

        flagL6_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL6_6.setToolTipText("");
        flagL6_6.setName("flag_L_36"); // NOI18N
        flagL6_6.setNextFocusableComponent(flagL1_1);

        scoreL6_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL6_6.setName("score_L_36"); // NOI18N
        scoreL6_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator6_6.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator6_6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator6_6.setText("-");
        separator6_6.setName("separator_36"); // NOI18N
        separator6_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV6_6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV6_6.setName("score_V_36"); // NOI18N
        scoreV6_6.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV6_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV6_6.setName("flag_V_36"); // NOI18N

        javax.swing.GroupLayout group6Layout = new javax.swing.GroupLayout(group6);
        group6.setLayout(group6Layout);
        group6Layout.setHorizontalGroup(
            group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(group6Layout.createSequentialGroup()
                            .addComponent(flagL5_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(scoreL5_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator5_6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV5_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flagV5_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(group6Layout.createSequentialGroup()
                                .addComponent(flagL1_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL1_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator1_6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV1_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV1_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group6Layout.createSequentialGroup()
                                .addComponent(flagL2_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL2_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator2_6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV2_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV2_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group6Layout.createSequentialGroup()
                                .addComponent(flagL3_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL3_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator3_6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV3_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV3_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group6Layout.createSequentialGroup()
                                .addComponent(flagL4_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL4_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator4_6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV4_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV4_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(group6Layout.createSequentialGroup()
                                .addComponent(flagL6_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreL6_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(separator6_6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scoreV6_6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(flagV6_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(TitleGroup_6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        group6Layout.setVerticalGroup(
            group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(group6Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(TitleGroup_6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_6)
                        .addComponent(separator1_6)
                        .addComponent(scoreV1_6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL2_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV2_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL2_6)
                        .addComponent(separator2_6)
                        .addComponent(scoreV2_6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL3_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV3_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL3_6)
                        .addComponent(separator3_6)
                        .addComponent(scoreV3_6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL4_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV4_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL4_6)
                        .addComponent(separator4_6)
                        .addComponent(scoreV4_6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagV5_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL5_6)
                            .addComponent(separator5_6)
                            .addComponent(scoreV5_6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(flagL5_6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL6_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV6_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(group6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL6_6)
                        .addComponent(separator6_6)
                        .addComponent(scoreV6_6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        playoffs.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        TitleGroup_7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_7.setText("Octavos");
        TitleGroup_7.setFocusCycleRoot(true);
        TitleGroup_7.setFocusable(false);

        TitleGroup_8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_8.setText("Cuartos");
        TitleGroup_8.setFocusCycleRoot(true);
        TitleGroup_8.setFocusable(false);

        TitleGroup_9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_9.setText("Semis");
        TitleGroup_9.setFocusCycleRoot(true);
        TitleGroup_9.setFocusable(false);

        TitleGroup_11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_11.setText("Semis");
        TitleGroup_11.setFocusCycleRoot(true);
        TitleGroup_11.setFocusable(false);

        TitleGroup_12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_12.setText("Cuartos");
        TitleGroup_12.setFocusCycleRoot(true);
        TitleGroup_12.setFocusable(false);

        TitleGroup_13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TitleGroup_13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_13.setText("Octavos");
        TitleGroup_13.setFocusCycleRoot(true);
        TitleGroup_13.setFocusable(false);

        javax.swing.GroupLayout PlayoffTitlesLayout = new javax.swing.GroupLayout(PlayoffTitles);
        PlayoffTitles.setLayout(PlayoffTitlesLayout);
        PlayoffTitlesLayout.setHorizontalGroup(
            PlayoffTitlesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PlayoffTitlesLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(TitleGroup_7, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(TitleGroup_8, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(TitleGroup_9, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(TitleGroup_11, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addComponent(TitleGroup_12, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addComponent(TitleGroup_13, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        PlayoffTitlesLayout.setVerticalGroup(
            PlayoffTitlesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(TitleGroup_11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PlayoffTitlesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(TitleGroup_7)
                .addComponent(TitleGroup_8)
                .addComponent(TitleGroup_9)
                .addComponent(TitleGroup_12)
                .addComponent(TitleGroup_13))
        );

        flag8s_2_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_2_1.setToolTipText("");
        flag8s_2_1.setName("flag_L_37"); // NOI18N
        flag8s_2_1.setNextFocusableComponent(flagL1_1);

        scoreL1_7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_7.setName("score_L_37"); // NOI18N
        scoreL1_7.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_7.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_7.setText("-");
        separator1_7.setName("separator_37"); // NOI18N
        separator1_7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_7.setName("score_V_37"); // NOI18N
        scoreV1_7.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_2_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_2_3.setName("flag_V_37"); // NOI18N

        flag8s_3_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_3_2.setName("flag_V_39"); // NOI18N

        scoreV1_9.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_9.setName("score_V_39"); // NOI18N
        scoreV1_9.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_3_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_3_1.setName("flag_V_38"); // NOI18N

        scoreV1_8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_8.setName("score_V_38"); // NOI18N
        scoreV1_8.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_8.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_8.setText("-");
        separator1_8.setName("separator_38"); // NOI18N
        separator1_8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreL1_8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_8.setName("score_L_38"); // NOI18N
        scoreL1_8.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_1_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_1_4.setToolTipText("");
        flag8s_1_4.setName("flag_L_38"); // NOI18N
        flag8s_1_4.setNextFocusableComponent(flagL1_1);

        flag8s_1_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_1_2.setToolTipText("");
        flag8s_1_2.setName("flag_L_39"); // NOI18N
        flag8s_1_2.setNextFocusableComponent(flagL1_1);

        scoreL1_9.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_9.setName("score_L_39"); // NOI18N
        scoreL1_9.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_9.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_9.setText("-");
        separator1_9.setName("separator_39"); // NOI18N
        separator1_9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_10.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_10.setName("score_V_40"); // NOI18N
        scoreV1_10.setNextFocusableComponent(scoreL1_21);
        scoreV1_10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_2_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_2_5.setName("flag_V_40"); // NOI18N

        separator1_10.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_10.setText("-");
        separator1_10.setName("separator_40"); // NOI18N
        separator1_10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreL1_10.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_10.setName("score_L_40"); // NOI18N
        scoreL1_10.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_1_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_1_6.setToolTipText("");
        flag8s_1_6.setName("flag_L_40"); // NOI18N
        flag8s_1_6.setNextFocusableComponent(flagL1_1);

        javax.swing.GroupLayout Left8sLayout = new javax.swing.GroupLayout(Left8s);
        Left8s.setLayout(Left8sLayout);
        Left8sLayout.setHorizontalGroup(
            Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Left8sLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Left8sLayout.createSequentialGroup()
                        .addComponent(flag8s_1_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_10, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_10, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_10, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flag8s_2_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(Left8sLayout.createSequentialGroup()
                            .addComponent(flag8s_1_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreL1_8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator1_8, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV1_8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flag8s_3_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(Left8sLayout.createSequentialGroup()
                            .addComponent(flag8s_2_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreL1_7, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator1_7, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV1_7, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flag8s_2_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(Left8sLayout.createSequentialGroup()
                        .addComponent(flag8s_1_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_9, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flag8s_3_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Left8sLayout.setVerticalGroup(
            Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Left8sLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_2_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_2_3)
                    .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_7)
                        .addComponent(scoreV1_7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_1_4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_3_1)
                    .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_8)
                        .addComponent(scoreV1_8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_1_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_3_2)
                    .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_9)
                        .addComponent(scoreV1_9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(33, 33, 33)
                .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_1_6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_2_5)
                    .addGroup(Left8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_10)
                        .addComponent(scoreV1_10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        scoreL1_11.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_11.setName("score_L_45"); // NOI18N
        scoreL1_11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagL1_11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_11.setName("flag_L_45"); // NOI18N

        separator1_11.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_11.setText("-");
        separator1_11.setName("separator_45"); // NOI18N
        separator1_11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_11.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_11.setName("score_V_45"); // NOI18N
        scoreV1_11.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_11.setName("flag_V_45"); // NOI18N

        flagV1_12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_12.setName("flag_V_46"); // NOI18N

        scoreV1_12.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_12.setName("score_V_46"); // NOI18N
        scoreV1_12.setNextFocusableComponent(scoreL1_17);
        scoreV1_12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_12.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_12.setText("-");
        separator1_12.setName("separator_46"); // NOI18N
        separator1_12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreL1_12.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_12.setName("score_L_46"); // NOI18N
        scoreL1_12.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagL1_12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_12.setToolTipText("");
        flagL1_12.setName("flag_L_46"); // NOI18N

        javax.swing.GroupLayout Left4sLayout = new javax.swing.GroupLayout(Left4s);
        Left4s.setLayout(Left4sLayout);
        Left4sLayout.setHorizontalGroup(
            Left4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Left4sLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Left4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Left4sLayout.createSequentialGroup()
                        .addComponent(flagL1_11, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_11, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_11, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_11, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flagV1_11, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(Left4sLayout.createSequentialGroup()
                        .addComponent(flagL1_12, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_12, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_12, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flagV1_12, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Left4sLayout.setVerticalGroup(
            Left4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Left4sLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(Left4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_11)
                    .addGroup(Left4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_11)
                        .addComponent(scoreV1_11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
                .addGroup(Left4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_12)
                    .addGroup(Left4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_12)
                        .addComponent(scoreV1_12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        flagL1_13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_13.setToolTipText("");
        flagL1_13.setName("flag_L_49"); // NOI18N

        scoreL1_13.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_13.setName("score_L_49"); // NOI18N
        scoreL1_13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_13.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_13.setText("-");
        separator1_13.setName("separator_49"); // NOI18N
        separator1_13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_13.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_13.setName("score_V_49"); // NOI18N
        scoreV1_13.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_13.setName("flag_V_49"); // NOI18N

        flagL1_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_14.setToolTipText("");
        flagL1_14.setName("flag_L_50"); // NOI18N

        scoreL1_14.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_14.setName("score_L_50"); // NOI18N
        scoreL1_14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_14.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_14.setText("-");
        separator1_14.setName("separator_50"); // NOI18N
        separator1_14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_14.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_14.setName("score_V_50"); // NOI18N
        scoreV1_14.setNextFocusableComponent(scoreL1_15);
        scoreV1_14.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_14.setName("flag_V_50"); // NOI18N

        flagV1_15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_15.setName("flag_V_51"); // NOI18N

        scoreV1_15.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_15.setName("score_V_51"); // NOI18N
        scoreV1_15.setNextFocusableComponent(jButton2);
        scoreV1_15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_15.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_15.setText("-");
        separator1_15.setName("separator_51"); // NOI18N
        separator1_15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreL1_15.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_15.setName("score_L_51"); // NOI18N
        scoreL1_15.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagL1_15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_15.setToolTipText("");
        flagL1_15.setName("flag_L_51"); // NOI18N

        TitleGroup_10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        TitleGroup_10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TitleGroup_10.setText("Final");
        TitleGroup_10.setFocusCycleRoot(true);
        TitleGroup_10.setFocusable(false);

        javax.swing.GroupLayout CenterMatchesLayout = new javax.swing.GroupLayout(CenterMatches);
        CenterMatches.setLayout(CenterMatchesLayout);
        CenterMatchesLayout.setHorizontalGroup(
            CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CenterMatchesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(flagL1_13, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scoreL1_13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(separator1_13, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scoreV1_13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(flagV1_13, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addComponent(flagL1_14, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scoreL1_14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(separator1_14, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scoreV1_14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(flagV1_14, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CenterMatchesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addGroup(CenterMatchesLayout.createSequentialGroup()
                        .addComponent(scoreL1_15, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_15, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_15, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(CenterMatchesLayout.createSequentialGroup()
                        .addComponent(flagL1_15, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(66, 66, 66)
                        .addComponent(flagV1_15, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(84, 84, 84))
            .addGroup(CenterMatchesLayout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addComponent(TitleGroup_10, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        CenterMatchesLayout.setVerticalGroup(
            CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CenterMatchesLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(TitleGroup_10)
                .addGap(14, 14, 14)
                .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_15)
                    .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_15)
                        .addComponent(scoreV1_15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagL1_14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(flagV1_14)
                        .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL1_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(separator1_14)
                            .addComponent(scoreV1_14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(flagL1_13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(flagV1_13)
                        .addGroup(CenterMatchesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scoreL1_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(separator1_13)
                            .addComponent(scoreV1_13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        flagL1_17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_17.setToolTipText("");
        flagL1_17.setName("flag_L_47"); // NOI18N

        scoreL1_17.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_17.setName("score_L_47"); // NOI18N
        scoreL1_17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_17.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_17.setText("-");
        separator1_17.setName("separator_47"); // NOI18N
        separator1_17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreV1_17.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_17.setName("score_V_47"); // NOI18N
        scoreV1_17.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagV1_17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_17.setName("flag_V_47"); // NOI18N

        flagV1_16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagV1_16.setName("flag_V_48"); // NOI18N

        scoreV1_16.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_16.setName("score_V_48"); // NOI18N
        scoreV1_16.setNextFocusableComponent(scoreL1_13);
        scoreV1_16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_16.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_16.setText("-");
        separator1_16.setName("separator_48"); // NOI18N
        separator1_16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreL1_16.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_16.setName("score_L_48"); // NOI18N
        scoreL1_16.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flagL1_16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagL1_16.setToolTipText("");
        flagL1_16.setName("flag_L_48"); // NOI18N

        javax.swing.GroupLayout Right4sLayout = new javax.swing.GroupLayout(Right4s);
        Right4s.setLayout(Right4sLayout);
        Right4sLayout.setHorizontalGroup(
            Right4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Right4sLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Right4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(Right4sLayout.createSequentialGroup()
                        .addComponent(flagL1_16, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_16, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_16, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_16, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flagV1_16, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(Right4sLayout.createSequentialGroup()
                        .addComponent(flagL1_17, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_17, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_17, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_17, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flagV1_17, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Right4sLayout.setVerticalGroup(
            Right4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Right4sLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(Right4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_17)
                    .addGroup(Right4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_17)
                        .addComponent(scoreV1_17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(77, 77, 77)
                .addGroup(Right4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flagL1_16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flagV1_16)
                    .addGroup(Right4sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_16)
                        .addComponent(scoreV1_16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        scoreV1_21.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_21.setName("score_V_41"); // NOI18N
        scoreV1_21.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        scoreV1_20.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_20.setName("score_V_42"); // NOI18N
        scoreV1_20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        separator1_20.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_20.setText("-");
        separator1_20.setName("separator_42"); // NOI18N
        separator1_20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        flag8s_1_5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_1_5.setToolTipText("");
        flag8s_1_5.setName("flag_L_42"); // NOI18N
        flag8s_1_5.setNextFocusableComponent(flagL1_1);

        scoreL1_21.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_21.setName("score_L_41"); // NOI18N
        scoreL1_21.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_3_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_3_3.setName("flag_V_41"); // NOI18N

        scoreV1_18.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_18.setName("score_V_44"); // NOI18N
        scoreV1_18.setNextFocusableComponent(scoreL1_11);
        scoreV1_18.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_2_6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_2_6.setName("flag_V_44"); // NOI18N

        scoreL1_18.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_18.setName("score_L_44"); // NOI18N
        scoreL1_18.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        scoreL1_20.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_20.setName("score_L_42"); // NOI18N
        scoreL1_20.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_2_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_2_4.setName("flag_V_42"); // NOI18N

        flag8s_2_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_2_2.setToolTipText("");
        flag8s_2_2.setName("flag_L_44"); // NOI18N
        flag8s_2_2.setNextFocusableComponent(flagL1_1);

        flag8s_1_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_1_1.setToolTipText("");
        flag8s_1_1.setName("flag_L_43"); // NOI18N
        flag8s_1_1.setNextFocusableComponent(flagL1_1);

        separator1_21.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_21.setText("-");
        separator1_21.setName("separator_41"); // NOI18N
        separator1_21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        separator1_18.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_18.setText("-");
        separator1_18.setName("separator_44"); // NOI18N
        separator1_18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        separator1_19.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        separator1_19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        separator1_19.setText("-");
        separator1_19.setName("separator_43"); // NOI18N
        separator1_19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                separatorMouseClicked(evt);
            }
        });

        scoreL1_19.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreL1_19.setName("score_L_43"); // NOI18N
        scoreL1_19.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        scoreV1_19.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        scoreV1_19.setName("score_V_43"); // NOI18N
        scoreV1_19.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                scoreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                scoreFocusLost(evt);
            }
        });

        flag8s_1_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_1_3.setToolTipText("");
        flag8s_1_3.setName("flag_L_41"); // NOI18N
        flag8s_1_3.setNextFocusableComponent(flagL1_1);

        flag8s_3_4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flag8s_3_4.setName("flag_V_43"); // NOI18N

        javax.swing.GroupLayout Right8sLayout = new javax.swing.GroupLayout(Right8s);
        Right8s.setLayout(Right8sLayout);
        Right8sLayout.setHorizontalGroup(
            Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Right8sLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(Right8sLayout.createSequentialGroup()
                            .addComponent(flag8s_2_2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreL1_18, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator1_18, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV1_18, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flag8s_2_6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(Right8sLayout.createSequentialGroup()
                            .addComponent(flag8s_1_1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreL1_19, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(separator1_19, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreV1_19, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(flag8s_3_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(Right8sLayout.createSequentialGroup()
                        .addComponent(flag8s_1_5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_20, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_20, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_20, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flag8s_2_4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(Right8sLayout.createSequentialGroup()
                        .addComponent(flag8s_1_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreL1_21, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(separator1_21, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scoreV1_21, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(flag8s_3_3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        Right8sLayout.setVerticalGroup(
            Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Right8sLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_1_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_3_3)
                    .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_21)
                        .addComponent(scoreV1_21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(33, 33, 33)
                .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_1_5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_2_4)
                    .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_20)
                        .addComponent(scoreV1_20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_1_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_3_4)
                    .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_19)
                        .addComponent(scoreV1_19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(flag8s_2_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(flag8s_2_6)
                    .addGroup(Right8sLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(scoreL1_18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(separator1_18)
                        .addComponent(scoreV1_18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        flagWinner.setIcon(new javax.swing.ImageIcon(getClass().getResource("/saac/_unknown.png"))); // NOI18N
        flagWinner.setToolTipText("");
        flagWinner.setName("flagWinner"); // NOI18N

        WinnerTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        WinnerTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        WinnerTitle.setText("Ganador");
        WinnerTitle.setFocusCycleRoot(true);
        WinnerTitle.setFocusable(false);
        WinnerTitle.setName("WinnerTitle"); // NOI18N

        javax.swing.GroupLayout playoffsLayout = new javax.swing.GroupLayout(playoffs);
        playoffs.setLayout(playoffsLayout);
        playoffsLayout.setHorizontalGroup(
            playoffsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playoffsLayout.createSequentialGroup()
                .addComponent(Left8s, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Left4s, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(playoffsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(playoffsLayout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(WinnerTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(flagWinner, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(playoffsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CenterMatches, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Right4s, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Right8s, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(playoffsLayout.createSequentialGroup()
                .addComponent(PlayoffTitles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        playoffsLayout.setVerticalGroup(
            playoffsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(playoffsLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(PlayoffTitles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(playoffsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Right8s, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(playoffsLayout.createSequentialGroup()
                        .addGroup(playoffsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Left8s, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Left4s, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(playoffsLayout.createSequentialGroup()
                                .addComponent(CenterMatches, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(23, 23, 23)
                                .addGroup(playoffsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(WinnerTitle)
                                    .addComponent(flagWinner)))
                            .addComponent(Right4s, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jButton1.setMnemonic('s');
        jButton1.setLabel("Simular");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setMnemonic('e');
        jButton2.setText("Estadísticas");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout formLayout = new javax.swing.GroupLayout(form);
        form.setLayout(formLayout);
        formLayout.setHorizontalGroup(
            formLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(formLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(formLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(formLayout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addGap(303, 303, 303)
                        .addComponent(jButton1))
                    .addGroup(formLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(playoffs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(formLayout.createSequentialGroup()
                            .addComponent(group1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(group2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(group3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(group4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(group5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(group6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        formLayout.setVerticalGroup(
            formLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(formLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(formLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(group5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(group6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(group4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(group3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(group2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(group1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(playoffs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(formLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(form, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(form, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private static void updateComponentsMatch(Match match, boolean changeFlags){
        ((JLabel)componentMap.get("separator_"+match.getNumber())).setToolTipText("Partido: "+match.getNumber());
        if(match.isFixed()){
            ((JLabel)componentMap.get("separator_"+match.getNumber())).setForeground(Color.GRAY);
            ((JTextField)componentMap.get("score_L_"+match.getNumber())).setEnabled(false);        
            ((JTextField)componentMap.get("score_V_"+match.getNumber())).setEnabled(false);
        }
        else{
            ((JLabel)componentMap.get("separator_"+match.getNumber())).setForeground(Color.BLACK);
            ((JTextField)componentMap.get("score_L_"+match.getNumber())).setEnabled(true);        
            ((JTextField)componentMap.get("score_V_"+match.getNumber())).setEnabled(true);
        }
        if (match.isPlayed()){
            ((JTextField)componentMap.get("score_L_"+match.getNumber())).setText(""+match.getGoalsLocal());
            ((JTextField)componentMap.get("score_V_"+match.getNumber())).setText(""+match.getGoalsVisitor());
            ((JLabel)componentMap.get("separator_"+match.getNumber())).setText(""+match.getSign());
        }
        if(changeFlags){
            //flag_LOCAL/VISITOR_CATEGORY_number
            ((JLabel)componentMap.get("flag_L_"+match.getNumber())).setIcon(match.getLocal().getFlag());
            ((JLabel)componentMap.get("flag_V_"+match.getNumber())).setIcon(match.getVisitor().getFlag());
            ((JLabel)componentMap.get("flag_L_"+match.getNumber())).setToolTipText(match.getLocal().getNameFull());
            ((JLabel)componentMap.get("flag_V_"+match.getNumber())).setToolTipText(match.getVisitor().getNameFull());
        }
    }
    
    public static TreeMap<String, Team> getTeams(){
        return teams;
    }
    
    public static Match[] getMatches(){
        return matches;
    }
    
    public static void simulateChampionship(boolean updateFrame, int fromCategory){
        //form.setVisible(false);
        
        // Play group matches
        if(fromCategory < 1){
            for(int i = 0; i < 36; i++){
                matches[i].play(true); 
            
                if (updateFrame)
                    updateComponentsMatch(matches[i], false);
            }
        }
        
        if(fromCategory < 7) {
            Iterator it = teams.entrySet().iterator();
            while(it.hasNext())
                ((Team)((Map.Entry)it.next()).getValue()).reset();

            for(int i = 0; i < 36; i++) { 
                // Add group points
                matches[i].getLocal().setPoints(matches[i].getLocal().getPoints()+matches[i].getPointsLocal());
                matches[i].getVisitor().setPoints(matches[i].getVisitor().getPoints()+matches[i].getPointsVisitor());            
            }
            
            // Set 8s matches
            Team[][] groupedCountries = new Team[6][4];
            for(int g = 1; g <= 6; g++){
                Team[] group = getGroup(g);
                Arrays.sort(group, ascGroups);
                groupedCountries[g-1] = group;
                group[0].setClassificationGroups(0);            
                group[1].setClassificationGroups(1);
                group[2].setClassificationGroups(2);           
                group[3].setClassificationGroups(3);

                group[1].setRoundMax(1);
                group[2].setRoundMax(1);         
                group[3].setRoundMax(1);
            }
            Team[] thirds = getThirds();
            Arrays.sort(thirds, ascThirds);
            thirds[0].setRoundMax(0);
            thirds[1].setRoundMax(0);

            //TODO REVISAR ORDENACIÓN (introducir datos reales y ver si clasifica bien)

            // Local[Group-1][4-position], Visitor[Group-1][4-position]
            // TODO (Calcular bien la posición de los terceros)
            matches[36] = resetMatch(new Match(7, 37, groupedCountries[1-1][4-2], groupedCountries[3-1][4-2]));
            matches[37] = resetMatch(new Match(7, 38, groupedCountries[4-1][4-1], thirds[5])); // Groups 2/5/6
            matches[38] = resetMatch(new Match(7, 39, groupedCountries[2-1][4-1], thirds[4])); // Groups 1/3/4
            matches[39] = resetMatch(new Match(7, 40, groupedCountries[6-1][4-1], groupedCountries[5-1][4-2]));
            matches[40] = resetMatch(new Match(7, 41, groupedCountries[3-1][4-1], thirds[3])); // Groups 1/2/6
            matches[41] = resetMatch(new Match(7, 42, groupedCountries[5-1][4-1], groupedCountries[4-1][4-2]));
            matches[42] = resetMatch(new Match(7, 43, groupedCountries[1-1][4-1], thirds[2])); // Groups 3/4/5
            matches[43] = resetMatch(new Match(7, 44, groupedCountries[2-1][4-2], groupedCountries[6-1][4-2]));
        
            // Play 8s matches
            for(int i = 36; i <= 43; i++){
                matches[i].play(false);
                matches[i].getWinner().setRoundMax(2);
                if (updateFrame)
                    updateComponentsMatch(matches[i], true);
            }      
        }
        
        if(fromCategory < 8){            
            // Set 4s matches
            matches[44] = resetMatch(new Match(8, 45, matches[36].getWinner(), matches[37].getWinner()));
            matches[45] = resetMatch(new Match(8, 46, matches[38].getWinner(), matches[39].getWinner()));
            matches[46] = resetMatch(new Match(8, 47, matches[40].getWinner(), matches[41].getWinner()));
            matches[47] = resetMatch(new Match(8, 48, matches[42].getWinner(), matches[43].getWinner()));

            // Play 4s matches
            for(int i = 44; i <= 47; i++){
                matches[i].play(false);
                matches[i].getWinner().setRoundMax(3);
                if (updateFrame)
                    updateComponentsMatch(matches[i], true);
            }
        }
        
        
        if(fromCategory < 9){
            // Set 2s matches
            matches[48] = resetMatch(new Match(9, 49, matches[44].getWinner(), matches[45].getWinner()));
            matches[49] = resetMatch(new Match(9, 50, matches[46].getWinner(), matches[47].getWinner()));
        
            // Play 2s matches
            for(int i = 48; i <= 49; i++){
                matches[i].play(false);
                matches[i].getWinner().setRoundMax(4);
                if (updateFrame)
                    updateComponentsMatch(matches[i], true);
            }

        }
    
        if(fromCategory < 10){            
            // Set final
            matches[50] = resetMatch(new Match(10, 51, matches[48].getWinner(), matches[49].getWinner()));
            
            // Play Final
            matches[50].play(false);
            matches[50].getWinner().setRoundMax(5);
            if (updateFrame)
                updateComponentsMatch(matches[50], true);
        }
        
        if (updateFrame){
            ((JLabel)componentMap.get("flagWinner")).setIcon(matches[50].getWinner().getFlag());
            ((JLabel)componentMap.get("WinnerTitle")).setText(matches[50].getWinner().getNameFull());
        }
        //form.setVisible(true);
    }
    
    private static Match resetMatch(Match match){ 
        if(matches[match.getNumber()-1] == null)
            return match;
        
        if(matches[match.getNumber()-1].isFixed() && matches[match.getNumber()-1].equals(match)){
            match.setResult(matches[match.getNumber()-1].getGoalsLocal(), matches[match.getNumber()-1].getGoalsVisitor());
            match.setFixed(true);
        }
        return match;        
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (teams.isEmpty())
            return;
               
        simulateChampionship(true, 0);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        if(statisticsFrame == null)
            statisticsFrame = new Statistics();
        
        if(!statisticsFrame.isVisible())
            statisticsFrame.setVisible(true);
        else
            statisticsFrame.requestFocus();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void separatorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_separatorMouseClicked
        // TODO add your handling code here:
        
        int matchNumber = Integer.parseInt(((JLabel)evt.getSource()).getName().substring(10))-1;        
        storeResult(matchNumber+1, true);
        
        if(matches[matchNumber].isPlayed()){
            if(matches[matchNumber].isFixed())
                matches[matchNumber].setFixed(false);
            else
                matches[matchNumber].setFixed(true);

            updateComponentsMatch(matches[matchNumber], false);
        }
        
    }//GEN-LAST:event_separatorMouseClicked

    private void storeResult(int matchNumber, boolean localPreference) {
    int goalsLocal = Integer.parseInt(((JTextField)componentMap.get("score_L_"+matchNumber)).getText());
        int goalsVisitor = Integer.parseInt(((JTextField)componentMap.get("score_V_"+matchNumber)).getText());
        
        if(matches[matchNumber-1].getGoalsLocal() != goalsLocal || matches[matchNumber-1].getGoalsVisitor() != goalsVisitor){
            if(goalsLocal == goalsVisitor && matches[matchNumber-1].getCategory()>6){
                if(!localPreference)
                    goalsVisitor++;
                else
                    goalsLocal++;
            }
                
            
            matches[matchNumber-1].setResult(goalsLocal, goalsVisitor);
            updateComponentsMatch(matches[matchNumber-1], false);
            simulateChampionship(true, matches[matchNumber-1].getCategory());
        }
        
    }
    
    private void scoreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_scoreFocusLost
        int matchNumber = Integer.parseInt(((JTextField)evt.getSource()).getName().substring(8));
        if(((JTextField)evt.getSource()).getName().contains("_L_"))
            storeResult(matchNumber, false);                    
        else
            storeResult(matchNumber, true);
    }//GEN-LAST:event_scoreFocusLost

    private void scoreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_scoreFocusGained
        ((JTextField)evt.getSource()).selectAll();
    }//GEN-LAST:event_scoreFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Simulator().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel CenterMatches;
    private javax.swing.JPanel Left4s;
    private javax.swing.JPanel Left8s;
    private javax.swing.JPanel PlayoffTitles;
    private javax.swing.JPanel Right4s;
    private javax.swing.JPanel Right8s;
    private javax.swing.JLabel TitleGroup_1;
    private javax.swing.JLabel TitleGroup_10;
    private javax.swing.JLabel TitleGroup_11;
    private javax.swing.JLabel TitleGroup_12;
    private javax.swing.JLabel TitleGroup_13;
    private javax.swing.JLabel TitleGroup_2;
    private javax.swing.JLabel TitleGroup_3;
    private javax.swing.JLabel TitleGroup_4;
    private javax.swing.JLabel TitleGroup_5;
    private javax.swing.JLabel TitleGroup_6;
    private javax.swing.JLabel TitleGroup_7;
    private javax.swing.JLabel TitleGroup_8;
    private javax.swing.JLabel TitleGroup_9;
    private javax.swing.JLabel WinnerTitle;
    private javax.swing.JLabel flag8s_1_1;
    private javax.swing.JLabel flag8s_1_2;
    private javax.swing.JLabel flag8s_1_3;
    private javax.swing.JLabel flag8s_1_4;
    private javax.swing.JLabel flag8s_1_5;
    private javax.swing.JLabel flag8s_1_6;
    private javax.swing.JLabel flag8s_2_1;
    private javax.swing.JLabel flag8s_2_2;
    private javax.swing.JLabel flag8s_2_3;
    private javax.swing.JLabel flag8s_2_4;
    private javax.swing.JLabel flag8s_2_5;
    private javax.swing.JLabel flag8s_2_6;
    private javax.swing.JLabel flag8s_3_1;
    private javax.swing.JLabel flag8s_3_2;
    private javax.swing.JLabel flag8s_3_3;
    private javax.swing.JLabel flag8s_3_4;
    private javax.swing.JLabel flagL1_1;
    private javax.swing.JLabel flagL1_11;
    private javax.swing.JLabel flagL1_12;
    private javax.swing.JLabel flagL1_13;
    private javax.swing.JLabel flagL1_14;
    private javax.swing.JLabel flagL1_15;
    private javax.swing.JLabel flagL1_16;
    private javax.swing.JLabel flagL1_17;
    private javax.swing.JLabel flagL1_2;
    private javax.swing.JLabel flagL1_3;
    private javax.swing.JLabel flagL1_4;
    private javax.swing.JLabel flagL1_5;
    private javax.swing.JLabel flagL1_6;
    private javax.swing.JLabel flagL2_1;
    private javax.swing.JLabel flagL2_2;
    private javax.swing.JLabel flagL2_3;
    private javax.swing.JLabel flagL2_4;
    private javax.swing.JLabel flagL2_5;
    private javax.swing.JLabel flagL2_6;
    private javax.swing.JLabel flagL3_1;
    private javax.swing.JLabel flagL3_2;
    private javax.swing.JLabel flagL3_3;
    private javax.swing.JLabel flagL3_4;
    private javax.swing.JLabel flagL3_5;
    private javax.swing.JLabel flagL3_6;
    private javax.swing.JLabel flagL4_1;
    private javax.swing.JLabel flagL4_2;
    private javax.swing.JLabel flagL4_3;
    private javax.swing.JLabel flagL4_4;
    private javax.swing.JLabel flagL4_5;
    private javax.swing.JLabel flagL4_6;
    private javax.swing.JLabel flagL5_1;
    private javax.swing.JLabel flagL5_2;
    private javax.swing.JLabel flagL5_3;
    private javax.swing.JLabel flagL5_4;
    private javax.swing.JLabel flagL5_5;
    private javax.swing.JLabel flagL5_6;
    private javax.swing.JLabel flagL6_1;
    private javax.swing.JLabel flagL6_2;
    private javax.swing.JLabel flagL6_3;
    private javax.swing.JLabel flagL6_4;
    private javax.swing.JLabel flagL6_5;
    private javax.swing.JLabel flagL6_6;
    private javax.swing.JLabel flagV1_1;
    private javax.swing.JLabel flagV1_11;
    private javax.swing.JLabel flagV1_12;
    private javax.swing.JLabel flagV1_13;
    private javax.swing.JLabel flagV1_14;
    private javax.swing.JLabel flagV1_15;
    private javax.swing.JLabel flagV1_16;
    private javax.swing.JLabel flagV1_17;
    private javax.swing.JLabel flagV1_2;
    private javax.swing.JLabel flagV1_3;
    private javax.swing.JLabel flagV1_4;
    private javax.swing.JLabel flagV1_5;
    private javax.swing.JLabel flagV1_6;
    private javax.swing.JLabel flagV2_1;
    private javax.swing.JLabel flagV2_2;
    private javax.swing.JLabel flagV2_3;
    private javax.swing.JLabel flagV2_4;
    private javax.swing.JLabel flagV2_5;
    private javax.swing.JLabel flagV2_6;
    private javax.swing.JLabel flagV3_1;
    private javax.swing.JLabel flagV3_2;
    private javax.swing.JLabel flagV3_3;
    private javax.swing.JLabel flagV3_4;
    private javax.swing.JLabel flagV3_5;
    private javax.swing.JLabel flagV3_6;
    private javax.swing.JLabel flagV4_1;
    private javax.swing.JLabel flagV4_2;
    private javax.swing.JLabel flagV4_3;
    private javax.swing.JLabel flagV4_4;
    private javax.swing.JLabel flagV4_5;
    private javax.swing.JLabel flagV4_6;
    private javax.swing.JLabel flagV5_1;
    private javax.swing.JLabel flagV5_2;
    private javax.swing.JLabel flagV5_3;
    private javax.swing.JLabel flagV5_4;
    private javax.swing.JLabel flagV5_5;
    private javax.swing.JLabel flagV5_6;
    private javax.swing.JLabel flagV6_1;
    private javax.swing.JLabel flagV6_2;
    private javax.swing.JLabel flagV6_3;
    private javax.swing.JLabel flagV6_4;
    private javax.swing.JLabel flagV6_5;
    private javax.swing.JLabel flagV6_6;
    private javax.swing.JLabel flagWinner;
    private javax.swing.JPanel form;
    private javax.swing.JPanel group1;
    private javax.swing.JPanel group2;
    private javax.swing.JPanel group3;
    private javax.swing.JPanel group4;
    private javax.swing.JPanel group5;
    private javax.swing.JPanel group6;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel playoffs;
    private javax.swing.JTextField scoreL1_1;
    private javax.swing.JTextField scoreL1_10;
    private javax.swing.JTextField scoreL1_11;
    private javax.swing.JTextField scoreL1_12;
    private javax.swing.JTextField scoreL1_13;
    private javax.swing.JTextField scoreL1_14;
    private javax.swing.JTextField scoreL1_15;
    private javax.swing.JTextField scoreL1_16;
    private javax.swing.JTextField scoreL1_17;
    private javax.swing.JTextField scoreL1_18;
    private javax.swing.JTextField scoreL1_19;
    private javax.swing.JTextField scoreL1_2;
    private javax.swing.JTextField scoreL1_20;
    private javax.swing.JTextField scoreL1_21;
    private javax.swing.JTextField scoreL1_3;
    private javax.swing.JTextField scoreL1_4;
    private javax.swing.JTextField scoreL1_5;
    private javax.swing.JTextField scoreL1_6;
    private javax.swing.JTextField scoreL1_7;
    private javax.swing.JTextField scoreL1_8;
    private javax.swing.JTextField scoreL1_9;
    private javax.swing.JTextField scoreL2_1;
    private javax.swing.JTextField scoreL2_2;
    private javax.swing.JTextField scoreL2_3;
    private javax.swing.JTextField scoreL2_4;
    private javax.swing.JTextField scoreL2_5;
    private javax.swing.JTextField scoreL2_6;
    private javax.swing.JTextField scoreL3_1;
    private javax.swing.JTextField scoreL3_2;
    private javax.swing.JTextField scoreL3_3;
    private javax.swing.JTextField scoreL3_4;
    private javax.swing.JTextField scoreL3_5;
    private javax.swing.JTextField scoreL3_6;
    private javax.swing.JTextField scoreL4_1;
    private javax.swing.JTextField scoreL4_2;
    private javax.swing.JTextField scoreL4_3;
    private javax.swing.JTextField scoreL4_4;
    private javax.swing.JTextField scoreL4_5;
    private javax.swing.JTextField scoreL4_6;
    private javax.swing.JTextField scoreL5_1;
    private javax.swing.JTextField scoreL5_2;
    private javax.swing.JTextField scoreL5_3;
    private javax.swing.JTextField scoreL5_4;
    private javax.swing.JTextField scoreL5_5;
    private javax.swing.JTextField scoreL5_6;
    private javax.swing.JTextField scoreL6_1;
    private javax.swing.JTextField scoreL6_2;
    private javax.swing.JTextField scoreL6_3;
    private javax.swing.JTextField scoreL6_4;
    private javax.swing.JTextField scoreL6_5;
    private javax.swing.JTextField scoreL6_6;
    private javax.swing.JTextField scoreV1_1;
    private javax.swing.JTextField scoreV1_10;
    private javax.swing.JTextField scoreV1_11;
    private javax.swing.JTextField scoreV1_12;
    private javax.swing.JTextField scoreV1_13;
    private javax.swing.JTextField scoreV1_14;
    private javax.swing.JTextField scoreV1_15;
    private javax.swing.JTextField scoreV1_16;
    private javax.swing.JTextField scoreV1_17;
    private javax.swing.JTextField scoreV1_18;
    private javax.swing.JTextField scoreV1_19;
    private javax.swing.JTextField scoreV1_2;
    private javax.swing.JTextField scoreV1_20;
    private javax.swing.JTextField scoreV1_21;
    private javax.swing.JTextField scoreV1_3;
    private javax.swing.JTextField scoreV1_4;
    private javax.swing.JTextField scoreV1_5;
    private javax.swing.JTextField scoreV1_6;
    private javax.swing.JTextField scoreV1_7;
    private javax.swing.JTextField scoreV1_8;
    private javax.swing.JTextField scoreV1_9;
    private javax.swing.JTextField scoreV2_1;
    private javax.swing.JTextField scoreV2_2;
    private javax.swing.JTextField scoreV2_3;
    private javax.swing.JTextField scoreV2_4;
    private javax.swing.JTextField scoreV2_5;
    private javax.swing.JTextField scoreV2_6;
    private javax.swing.JTextField scoreV3_1;
    private javax.swing.JTextField scoreV3_2;
    private javax.swing.JTextField scoreV3_3;
    private javax.swing.JTextField scoreV3_4;
    private javax.swing.JTextField scoreV3_5;
    private javax.swing.JTextField scoreV3_6;
    private javax.swing.JTextField scoreV4_1;
    private javax.swing.JTextField scoreV4_2;
    private javax.swing.JTextField scoreV4_3;
    private javax.swing.JTextField scoreV4_4;
    private javax.swing.JTextField scoreV4_5;
    private javax.swing.JTextField scoreV4_6;
    private javax.swing.JTextField scoreV5_1;
    private javax.swing.JTextField scoreV5_2;
    private javax.swing.JTextField scoreV5_3;
    private javax.swing.JTextField scoreV5_4;
    private javax.swing.JTextField scoreV5_5;
    private javax.swing.JTextField scoreV5_6;
    private javax.swing.JTextField scoreV6_1;
    private javax.swing.JTextField scoreV6_2;
    private javax.swing.JTextField scoreV6_3;
    private javax.swing.JTextField scoreV6_4;
    private javax.swing.JTextField scoreV6_5;
    private javax.swing.JTextField scoreV6_6;
    private javax.swing.JLabel separator1_1;
    private javax.swing.JLabel separator1_10;
    private javax.swing.JLabel separator1_11;
    private javax.swing.JLabel separator1_12;
    private javax.swing.JLabel separator1_13;
    private javax.swing.JLabel separator1_14;
    private javax.swing.JLabel separator1_15;
    private javax.swing.JLabel separator1_16;
    private javax.swing.JLabel separator1_17;
    private javax.swing.JLabel separator1_18;
    private javax.swing.JLabel separator1_19;
    private javax.swing.JLabel separator1_2;
    private javax.swing.JLabel separator1_20;
    private javax.swing.JLabel separator1_21;
    private javax.swing.JLabel separator1_3;
    private javax.swing.JLabel separator1_4;
    private javax.swing.JLabel separator1_5;
    private javax.swing.JLabel separator1_6;
    private javax.swing.JLabel separator1_7;
    private javax.swing.JLabel separator1_8;
    private javax.swing.JLabel separator1_9;
    private javax.swing.JLabel separator2_1;
    private javax.swing.JLabel separator2_2;
    private javax.swing.JLabel separator2_3;
    private javax.swing.JLabel separator2_4;
    private javax.swing.JLabel separator2_5;
    private javax.swing.JLabel separator2_6;
    private javax.swing.JLabel separator3_1;
    private javax.swing.JLabel separator3_2;
    private javax.swing.JLabel separator3_3;
    private javax.swing.JLabel separator3_4;
    private javax.swing.JLabel separator3_5;
    private javax.swing.JLabel separator3_6;
    private javax.swing.JLabel separator4_1;
    private javax.swing.JLabel separator4_2;
    private javax.swing.JLabel separator4_3;
    private javax.swing.JLabel separator4_4;
    private javax.swing.JLabel separator4_5;
    private javax.swing.JLabel separator4_6;
    private javax.swing.JLabel separator5_1;
    private javax.swing.JLabel separator5_2;
    private javax.swing.JLabel separator5_3;
    private javax.swing.JLabel separator5_4;
    private javax.swing.JLabel separator5_5;
    private javax.swing.JLabel separator5_6;
    private javax.swing.JLabel separator6_1;
    private javax.swing.JLabel separator6_2;
    private javax.swing.JLabel separator6_3;
    private javax.swing.JLabel separator6_4;
    private javax.swing.JLabel separator6_5;
    private javax.swing.JLabel separator6_6;
    // End of variables declaration//GEN-END:variables
}
