/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saac;

/**
 *
 * @author Jose
 */
public class Match {
    private final int category;
    private final int number;     
    private final Team local;
    private final Team visitor;
    private int goalsLocal;
    private int goalsVisitor;
    private boolean fixed;
    private boolean played;
    
    Match(){
        category = 0;
        number = 0;
        local = null;
        visitor = null;
    }
    
    Match(int category, int number, Team local, Team visitor){
        this.category = category;
        this.number = number;
        this.local = local;
        this.visitor = visitor;
    }
    
    public void setFixed(boolean fixed){
        this.fixed = fixed;
    }
    
    public int getCategory(){
        return category;
    }
    
    public int getNumber(){
        return number;
    }
    
    public Team getLocal(){
        return local;
    }
    
    public Team getVisitor(){
        return visitor;
    }
    
    public int getGoalsLocal(){
        return goalsLocal;
    }
    
    public int getGoalsVisitor(){
        return goalsVisitor;
    }
    
    public int getPointsLocal(){
        if(goalsLocal > goalsVisitor)
            return 3;
        else if(goalsLocal == goalsVisitor)
            return 1;
        else
            return 0;
    }
    
    public int getPointsVisitor(){
        if(goalsLocal < goalsVisitor)
            return 3;
        else if(goalsLocal == goalsVisitor)
            return 1;
        else
            return 0;
    }
    
    public void setResult(int goalsLocal, int goalsVisitor) {
        this.goalsLocal = goalsLocal;
        this.goalsVisitor = goalsVisitor;
        played = true;
    }
    
    public void play(boolean allowDraw) {
        if (!fixed){            
            double l = local.getQuality()*2+Math.random();
            double v = visitor.getQuality()*2+Math.random();

            if(Math.abs(l-v) < 0.20){
                if(allowDraw){
                    goalsLocal = (int)(Math.random()*5);
                    goalsVisitor = goalsLocal;
                }
                else{
                    goalsLocal = (int) (Math.random()*5+1);
                    goalsVisitor = (int) (Math.random()*goalsLocal);
                }
            }
            else if(l > v) {
                goalsLocal = (int) (Math.random()*5+1);
                goalsVisitor = (int) (Math.random()*goalsLocal);
            }
            else{
                goalsVisitor = (int)(Math.random()*5+1);
                goalsLocal = (int) (Math.random()*goalsLocal);
            } 
            
            played = true;
            //System.out.println(l + " - " + v);
        }
    }
    
    public Team getWinner(){
        if (goalsLocal > goalsVisitor)
            return local;
        else if(goalsLocal < goalsVisitor)
            return visitor;
        else
            return null;
    }
    
    public char getSign(){
        if (goalsLocal > goalsVisitor)
            return '1';
        else if(goalsLocal < goalsVisitor)
            return '2';
        else
            return 'X';
    }
    
    public boolean equals(Match match){
        if(match == null)
            return false;
        
        if (match.getNumber() == number &&
            match.getLocal().equals(local) &&
            match.getVisitor().equals(visitor))
            return true;
        else
            return false;    
    }
    
    public boolean isFixed(){
        return fixed;
    }
    
    public boolean isPlayed(){
        return played;
    }
    
    public void setPlayed(boolean played){
        this.played = played;
    }
            
    
    public String toString(){
        String l = "null";
        String v = "null";
        if(local!=null)
            l=local.getNameFull();
        if(visitor!=null)
            v=visitor.getNameFull();                
        return number+") " +l+" "+goalsLocal+" - "+v+ " "+goalsVisitor;
    }
}
