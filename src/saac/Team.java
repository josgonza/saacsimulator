/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saac;

import javax.swing.ImageIcon;

/**
 *
 * @author Jose
 */
public class Team {
    private final String fullName;
    private final int group;    
    private double quality; //From 0 to 1.
    private final double uefaCoefficient;    
    private final String code;
    
    private ImageIcon flag;
    
    private int points;
    private int classificationGroups;
    private int roundMax;

    
    Team(String fullName, int group, double quality, double uefaCoefficient, String code){
        this.fullName = fullName;
        this.quality = quality;
        this.group = group;
        this.code = code;
        this.uefaCoefficient = uefaCoefficient;
    }
    
    public String getNameFull() {
        return fullName;
    }
    
    public String getCode() {
        return code;
    }
    
    public ImageIcon getFlag() {
        return flag;
    }
    
    public double getQuality(){
        return quality;
    }
    
    public void setQuality(double quality){
        this.quality = quality;
    }
    
    public void setFlag(ImageIcon flag){
        this.flag = flag;
    }

    public int getPoints(){
        return points;
    }
    
    public int getGroupId(){
        return group;
    }
    
    public String getGroupName(){
        return ""+(char)(64+group);
    }
    
    public void setPoints(int points){
        this.points = points;
    }
    
    
    public int getClassificationGroups(){
        return classificationGroups;
    }
    
    public void setClassificationGroups(int roundMax){
        this.classificationGroups = roundMax;
    }
    
    public void setRoundMax(int roundMax){
        this.roundMax = roundMax;
    }
    
    public int getRoundMax(){
        return roundMax;
    }    
    
    public boolean equals(Team country){
        if (country == null)
            return false;
        
        return fullName.equals(country.getNameFull());
    }
    
    public double getUefaCoefficient(){
        return uefaCoefficient;
    }
    
    public void reset(){
        points = 0;
        classificationGroups = 0;
    }
}
